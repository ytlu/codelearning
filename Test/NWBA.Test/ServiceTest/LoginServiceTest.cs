﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;
using NWBA.Logic.Interface.Service;

namespace NWBA.Test.ServiceTest
{
    [TestFixture]
    public class LoginServiceTest : NWBABaseTest
    {
        #region Private Members

        private ILoginService _loginService;

        #endregion


        #region Setup

        [TestFixtureSetUp]
        public void Setup()
        {
            _loginService = Resolve<ILoginService>();
        }

        #endregion


        #region Test Methods

        [Test]
        public void TestLogin()
        {
            bool isValidUser = _loginService.IsValidLogin(NWBATestData.LoginData.UserName,
                NWBATestData.LoginData.Password);
            isValidUser.Should().BeTrue("User with correct password should be valid");
        }

        #endregion
    }
}
