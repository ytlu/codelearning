﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NWBA.Test
{
    public class NWBATestData
    {
        public class LoginData
        {
            public static string UserName { get { return "user1"; } }
            public static string Password { get { return "test123"; } }
        }

        public class CommonData
        {
            public static string TextToBeEncrypt { get { return "originalText"; } }
        }
    }
}
