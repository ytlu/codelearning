﻿using System;
using System.Security.Cryptography.X509Certificates;
using FluentAssertions;
using NUnit.Framework;
using NWBA.Common;
using NWBA.Logic;

namespace NWBA.Test
{
    [TestFixture]
    public class CommonTest
    {
        [Test]
        public void TestChiperString()
        {
            var encryptString = CipherService.Encrypt(NWBATestData.CommonData.TextToBeEncrypt, NWBAConfig.ENCRY_KEY_VALUE);
            NWBATestData.CommonData.TextToBeEncrypt.Should().NotMatch(encryptString, "Encrypted password should not match with orginal password");
        }
    }
}
