﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using NWBA.Logic.DI;

namespace NWBA.Test
{
    public abstract class NWBABaseTest
    {
        private NWBALogicDIResolver _resolver;

        protected NWBABaseTest()
        {
            _resolver = new NWBALogicDIResolver(NWBALogicDIResolver.ResolverType.ClassLibrary);
            _resolver.Configure();
        }


        protected T Resolve<T>()
        {
            return _resolver.myContainer.Resolve<T>();
        }
    }
}
