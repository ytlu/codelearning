------------------------------------
PROJECT DESCRIPTION
------------------------------------
This project is a web application, which utilise MS SQL database as storage.  
This website will use Membership and role provider approach to authenticate user.

 
------------------------------------
DEPENDENCY AND REFERENCE
------------------------------------
This project will use some external library to aid with the development 
and cleaner code.

*  [Autofac](https://www.nuget.org/packages/Autofac.Mvc4)
   This library enable easy setup of dependency injection

*  [AutoMapper](https://www.nuget.org/packages/AutoMapper)
   This library provide easy object to object mapping

*  [Bootstrap](http://getbootstrap.com/)
   This library provides ready made styling and support mobile responsive


------------------------------------
ASSUMPTIONS
------------------------------------
*  The site is intent for user with JavaScript and cookie enabled only

*  Deposit money should not require charge, and will not be 
   count towards free transaction.

*  Money transfer only allow between your own accounts, 
   not other customer's account.

*  Date format should including hour, minute and second, 
   because it's transaction related
   
*  The payee list is shared between all accounts.

*  There will not be a page to create Payee, the list is preloaded
   using SQL script

*  A login record can only be administrator or customer, you cannot 
   share the same login between account type.

*  Administrator login will be perform first, if a login is identify
   as administator, system will redirect user to the admin screen

*  An administrator will not be able to view customer page, and customer
   will not be able to view administrator pages (this is done by using
   [Authorize(Roles = "Admin")] and [Authorize(Roles = "User")]


------------------------------------
PATTERN AND APPROACH
------------------------------------
1.  Onion Architecture
    This project uses [Onion Architecture](http://www.develop.com/onionarchitecture) 
    to certain extend. Due to the size of the project, there is no necessary 
    to break down repository and service into separate project
    The logic has been contain within NWBA.Logic to keep it simple.

2.  Dependency Injection (DI)
    The project will use DI, the setup can be found in
    NWBA.Web > App_Start > Bootstrap.cs
    DI is choosen to enable us to easily swap out the concrete class,
    as well as enable us to mock the service for automate testing.


------------------------------------
PROJECT STRUCTURE
------------------------------------
The solution is break down into 5 solution folders:
*  Common  - This folder contains projects with common functions 
             which does not have any dependency with any other project.

*  Data    - This folder contains projects which communicate with database, 
             it can be any type of data access layer, in this project, 
             it's Entity Framework

*  Domain  - This folder contains domain entity definition projects, 
             there are 2 projects,  one defines the interface of the domain, 
             and one for the real implementation of the domain.
             The reason for define 2 project is because I can at any point, 
             swap the implementation with different implementation depending 
             on the business requirement

*  Logic   - This folder contains the business logic. For simplistic,
             I have keep the repository and service within this folder
             Same approach as Domain layer, there should be interface and 
             implementation project
             
*  UI     - This folder will contain any UI layer project, 
             such as website, or Web API.


------------------------------------
CONFIGURE PROGRAM
------------------------------------
In order to run the website, there are several configuration required:
1.  Setup database
    On the root of the zip file, there is a TSQL file call NWBADB-CreateSQL.sql.
    Open the file and you will need to change the file path for the 
    database file and the log file. By default, it's pointing to 
    MS SQL data folder, please updated it to the folder of your choice.
    Once you have change the file path, connect to your database server 
    with the highest DB access, and run the TSQL.
    
    To check the setup, you can try to connect to database server with 
    * user: nwba
    * password: nwba123
    You should see a database called NWBADB been created.

2. Web.config
   Open web.config file in the root folder of NWBA.Web project, 
   and locate connectionStrings. There are 2 entries, 
   one for EF, and one for the membership provider:
   * NWBADBEntities
   * DefaultConnection
   
   You will need to update both entries with your database server.  

   If you are running database on your localhost, you don't need to 
   change anything, the username and password has been configured for you.
   If not, search for localhost, and replace datasource with your database server name.
   
   Example:
   <add name="DefaultConnection" connectionString="data source=localhost;
   Change to =>
   <add name="DefaultConnection" connectionString="data source=database-server-name;

3. Logging
   Logging is turn off on the website, to enable the login, navigate to NWBA.Web > Global.asax.cs
   Remove the #if !DEBUG OR
   Change your build mode to Release or any other mode other than Debug


------------------------------------
RUNNING PROGRAM
------------------------------------
To run the website, run Visual Studio as administrator, 
this project is setup to use local IIS instead of IIS express.
The solution is located in the root folder of the zip, call Assignment2.sln


1. Open As Customer
   Once you open the solution, set NWBA.Web project as default, compile, 
   and open the following URL in your browser:
   
   *  http://localhost/NWBA.Web/

   OR, just start the debug if you wish.

   There are 10 users been created, each users will have 3 accounts.
   The username and password are:
   *   User:       user1 [to user10, 10 test users]
   *   Password:   test123


2. Open As Adminstrator
   To access to the admin setction, open the following URL:
   *  http://localhost/NWBA.Web/Admin

   There is a admin user been created:
   *   User:       admin
   *   Password:   test123
