﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace NWBA.Common
{
    public class CipherService
    {
       

        #region Public Methods
        public static string Encrypt(string plainText, string salt)
        {
            byte[] hashedBytes;
            using (var cryptoService = new SHA1CryptoServiceProvider())
            {
                byte[] textWithSaltBytes = Encoding.UTF8.GetBytes(string.Concat(plainText, salt));
                hashedBytes = cryptoService.ComputeHash(textWithSaltBytes);
                cryptoService.Clear();
            }
            
            
            return Convert.ToBase64String(hashedBytes);
        }


        #endregion
    }
}
