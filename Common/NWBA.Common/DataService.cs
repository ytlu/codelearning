﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;

namespace NWBA.Common
{
    public sealed class DataService
    {
        #region Public Methods
        public static TInterface ConvertToObjectInterface<TSource, TInterface>(TSource source)
        {
            return Mapper.Map<TInterface>(source);
        }

        public static void VerifyNewRecordStatus(int rowAffected, string errorMsg)
        {
            VerifyRecordStatus(rowAffected, errorMsg, 1);
        }

        public static void VerifyUpdateRecordStatus(int rowAffected, string errorMsg)
        {
            VerifyRecordStatus(rowAffected, errorMsg, 0);
        }

        #endregion


        #region Helpr Methods
        private static void VerifyRecordStatus(int rowAffected, string errorMsg, int checkRowCount)
        {
            if (rowAffected < checkRowCount)
            {
                throw new Exception(errorMsg);
            }
        }

        #endregion
    }
}
