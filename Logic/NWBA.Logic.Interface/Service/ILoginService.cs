﻿namespace NWBA.Logic.Interface.Service
{
    public interface ILoginService
    {
        bool IsAdministrator(string userName);
        bool IsValidLogin(string userName, string password);
        void UpdatePassword(string userName, string currentPassword, string newPassword);
    }
}