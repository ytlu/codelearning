﻿using System.Collections.Generic;
using NWBA.Domain.Interface;

namespace NWBA.Logic.Interface.Service
{
    public interface IBillPayService
    {
        IAdminSchedulePaymentModel GetAdminBillPays(int selectedPage);
        IBillPayRecord GetBillPays(int customerID, int selectedPage);
        IBillPay GetBillPay(int customerID, int billPayID);
        int SaveBillPay(IBillPay billPay);

        void EnableBillPay(int billPayID);
        void PauseBillPay(int billPayID);
        void DeleteBillPay(int customerID, int billPayID);
    }
}
