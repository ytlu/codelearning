﻿using System.Collections;
using System.Collections.Generic;
using NWBA.Domain.Interface;

namespace NWBA.Logic.Interface.Service
{
    public interface ICustomerService
    {
        ICustomer GetCustomerByID(int customerID);
        ICustomerListModel GetCustomerList(int selectedPage);
        ICustomer GetCustomerByUserName(string userName);
        int GetCustomerIDByUserName(string userName);
        IAddress GetAddressByCustomerID(int customerID);
        IList<int> GetCustomerAccountNumberList(int customerID);
        int SaveCustomer(ICustomer customer);
        void UpdatePassword(int customerID, string userName, string currentPassword, string newPassword);
        IBillPayRecord GetBillPays(int customerID, int selectedPage);
    }
}