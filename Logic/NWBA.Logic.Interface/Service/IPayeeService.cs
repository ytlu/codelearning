﻿using System.Collections.Generic;
using NWBA.Domain.Interface;

namespace NWBA.Logic.Interface.Service
{
    public interface IPayeeService
    {
        IList<IPayee> GetPayess();
    }

}
