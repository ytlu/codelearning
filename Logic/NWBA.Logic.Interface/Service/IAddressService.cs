﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NWBA.Domain.Interface;

namespace NWBA.Logic.Interface.Service
{
    public interface IAddressService
    {
        IAddress GetAddressByCustomerID(int customerID);

        int SaveAddress(IAddress address);
    }
}
