﻿using NWBA.Domain.Interface;

namespace NWBA.Logic.Interface.Service
{
    public interface ITransactionService
    {
        
        int SaveTransaction(ITransaction transaction, IAccount account, out decimal newBalance);
        decimal TransferFund(ITransaction transaction, IAccount account);
    }
}