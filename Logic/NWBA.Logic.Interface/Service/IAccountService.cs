﻿using System.Collections.Generic;
using NWBA.Domain.Interface;

namespace NWBA.Logic.Interface.Service
{
    public interface IAccountService
    {
        IList<IAccount> GetCustomerAccounts(int customerID);
        IAccountTransaction GetAccountTransactions(int accountNumber, int selectedPage);
        ITransactionResult SaveTransaction(ITransaction transaction);
        
    }
}