﻿using NWBA.Domain.Interface;

namespace NWBA.Logic.Interface.Service
{
    public interface IPagingService
    {
        IRecordRangeModel GetRecordRange(int selectedPage);
        IPageModel BuildPageModel(int totalRecords, int selectedPage);
    }
}