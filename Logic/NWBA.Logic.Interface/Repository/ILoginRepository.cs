using NWBA.Domain.Interface;

namespace NWBA.Logic.Interface.Repository
{
    public interface ILoginRepository
    {
        bool IsAdministrator(string userName);
        bool IsValidLogin(string userName, string password);
        ILogin GetLoginByUserName(string userName);
        void UpdatePassword(int loginID, string newPassword);
    }
}