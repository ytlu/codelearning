using NWBA.Domain.Interface;

namespace NWBA.Logic.Interface.Repository
{
    public interface ITransactionRepository
    {
        int SaveTransaction(ITransaction transaction);
        int GetChargableTransactionCount(int accountNumber);
    }
}