using System.Collections;
using System.Collections.Generic;
using NWBA.Domain.Interface;

namespace NWBA.Logic.Repository
{
    public interface ICustomerRepository
    {
        ICustomer GetCustomerByID(int customerID);
        ICustomer GetCustomerByUserName(string userName);
        IList<ICustomer> GetCustomers(int recordFrom, int numberOfItem);
        int GetCustomerIDByUserName(string userName);
        IList<int> GetCustomerAccountList(int customerID);
        int SaveCustomer(ICustomer customer);
        void VerifyLoginByCustomerIDandUserName(int customerID, string userName);
        int TotalCustomerCount();
    }
}