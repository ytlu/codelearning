using NWBA.Domain.Interface;

namespace NWBA.Logic.Interface.Repository
{
    public interface IAddressRepository
    {
        IAddress GetAddressByID(int addressID);
        int SaveAddress(IAddress address);
        IAddress GetAddressByCustomerID(int customerID);
    }
}