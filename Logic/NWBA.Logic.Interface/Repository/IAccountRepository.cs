using System.Collections.Generic;
using System.Security.Cryptography;
using NWBA.Domain.Interface;

namespace NWBA.Logic.Interface.Repository
{
    public interface IAccountRepository
    {
        IList<IAccount> GetCustomerAccounts(int customerID);
        IAccount GetAccount(int accountNumber);
        int SaveAccount(IAccount account);
        IList<ITransaction> GetAccountTransactions(int accountNumber, int recordFrom, int numberOfItem);
        int TotalTransaction(int accountNumber);
        
    }
}