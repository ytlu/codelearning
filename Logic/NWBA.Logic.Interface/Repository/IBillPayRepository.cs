﻿using System.Collections.Generic;
using NWBA.Domain.Interface;

namespace NWBA.Logic.Interface.Repository
{
    public interface IBillPayRepository
    {
        IBillPay GetBillPay(int billPayID);
        IList<ICustomerPayBill> GetCustomerBillPays(int customerID, int recordFrom, int numberOfItem);
        IList<IAdminBillPayRecord> GetAdminBillPays(int recordFrom, int numberOfItem);
        int SaveBillPay(IBillPay billPay);
        int TotalBillPayCount(int customerID);
        int TotalAdminBillPayCount();
        bool IsValidBillPayRecord(int customerID, int billPayID);
        void EnableBillPay(int billPayID);
        void PauseBillPay(int billPayID);
        void DeleteBillPay(int billPayID);
    }
}
