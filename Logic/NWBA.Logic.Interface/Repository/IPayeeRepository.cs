﻿using System.Collections.Generic;
using NWBA.Domain.Interface;

namespace NWBA.Logic.Interface.Repository
{
    public interface IPayeeRepository
    {
        IList<IPayee> GetPayeeList();
    }
}
