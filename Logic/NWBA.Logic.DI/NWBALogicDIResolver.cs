﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using NWBA.Logic.Interface.Repository;
using NWBA.Logic.Interface.Service;
using NWBA.Logic.Repository;
using NWBA.Logic.Service;

namespace NWBA.Logic.DI
{
    public class NWBALogicDIResolver
    {

        #region Public Enum

        public enum ResolverType
        {
            ClassLibrary,
            MVC,
            WebAPI
        }

        #endregion


        #region Private Members

        private readonly ResolverType _resolverType;

        #endregion


        #region Constructor

        public NWBALogicDIResolver(ResolverType resolverType)
        {
            _resolverType = resolverType;
        }

        #endregion


        #region Public Properties

        public IContainer myContainer { get; private set; }

        #endregion


        #region Public Methods

        public void Configure()
        {
            ConfigureDI(null);
        }

        // Assemble is required to ensure the request depency resolve correctly
        public void ConfigureForWebApi(Assembly webAssembly)
        {
            ConfigureDI(webAssembly);
        }

        #endregion


        #region Private Methods

        private void ConfigureDI(Assembly assembly)
        {
            var builder = new ContainerBuilder();
            OnConfigure(builder, assembly);

            if (this.myContainer == null)
            {
                this.myContainer = builder.Build();
            }
            else
            {
                builder.Update(this.myContainer);
            }
        }

        private void OnConfigure(ContainerBuilder builder, Assembly assembly)
        {
            switch (_resolverType)
            {
                case ResolverType.ClassLibrary:
                    ConfigDependency(builder);
                    break;
                case ResolverType.MVC:
                    ConfigWebMVC(builder, assembly);
                    break;
                case ResolverType.WebAPI:
                    ConfigWebAPI(builder, assembly);
                    break;
            }
        }


        private void ConfigWebAPI(ContainerBuilder builder, Assembly assembly)
        {
            builder.RegisterApiControllers(assembly);
            ConfigDependency(builder);
        }

        private void ConfigWebMVC(ContainerBuilder builder, Assembly assembly)
        {
            builder.RegisterControllers(assembly);
            ConfigDependency(builder);
        }

        private void ConfigDependency(ContainerBuilder builder)
        {
            builder.RegisterType<AddressRepository>().As<IAddressRepository>();
            builder.RegisterType<LoginRepository>().As<ILoginRepository>();
            builder.RegisterType<CustomerRepository>().As<ICustomerRepository>();
            builder.RegisterType<AccountRepository>().As<IAccountRepository>();
            builder.RegisterType<TransactionRepository>().As<ITransactionRepository>();
            builder.RegisterType<PayeeRepository>().As<IPayeeRepository>();
            builder.RegisterType<BillPayRepository>().As<IBillPayRepository>();

            // Register the rest of the service, please note if the service is injected, make sure
            // you start with the one without any injection first
            builder.RegisterType<PagingService>().As<IPagingService>();
            builder.RegisterType<TransactionService>().As<ITransactionService>();
            builder.RegisterType<AddressService>().As<IAddressService>();
            builder.RegisterType<AccountService>().As<IAccountService>();
            builder.RegisterType<LoginService>().As<ILoginService>();
            builder.RegisterType<PayeeService>().As<IPayeeService>();
            builder.RegisterType<BillPayService>().As<IBillPayService>();
            builder.RegisterType<CustomerService>().As<ICustomerService>();
            builder.RegisterType<AdministrationService>().As<IAdministrationService>();
        }
        #endregion
    }
}
