﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;
using NWBA.Logic.Interface.Service;
using NWBA.Logic.Repository;
using NWBA.Logic.Service;

namespace NWBA.Logic.Security
{
    /// <summary>
    /// This class will return the role of the customer
    /// For milestone 1, every user will have role of User
    /// 
    /// This class will be updated in milestone 2 of the assign to return the admin role
    /// </summary>
    public class NWBARoleProvider : RoleProvider
    {
        #region Private Members

        private ILoginService _loginService;

        private ILoginService GetLoginService
        {
            get
            {
                if (_loginService == null)
                {
                    _loginService = new LoginService(new LoginRepository());
                }
                return _loginService;
            }

        }

        #endregion


        #region Override Public Methods

        public override string[] GetRolesForUser(string username)
        {
            string userRole = GetLoginRole(username);
            return new[] { userRole };
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            string userRole = GetLoginRole(username);
            return (userRole == roleName);
        }

        #endregion


        #region Helper Methods

        /// <summary>
        /// Get the role of the login user
        /// </summary>
        /// <param name="username">Username</param>
        /// <returns>Role of the login</returns>
        private string GetLoginRole(string username)
        {
            return (GetLoginService.IsAdministrator(username) ? "Admin" : "User");
        }

        #endregion


        #region No Implemented Methods
        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string ApplicationName { get; set; }

        #endregion
    }
}
