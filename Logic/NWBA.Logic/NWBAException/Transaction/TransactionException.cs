﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NWBA.Logic.NWBAException.Transaction
{
    /// <summary>
    /// This class contain the custom message for the transaction exception
    /// </summary>
    public class TransactionException : Exception
    {
        #region Transaction Error Enum Type

        public enum TransactionExceptionType
        {
            InsufficentFund,
            SaveFailed,
            TransferError,
            InvalidTransferAccount,
            TransferSameAccount
        }

        #endregion


        #region Constructor

        public TransactionException(TransactionExceptionType exceptionType)
            : base(BuildErrorMsg(exceptionType))
        {

        }

        #endregion


        #region Private Static Methods

        /// <summary>
        /// This method build the custom message for the transaction exception
        /// </summary>
        /// <param name="exceptionType">Transaction exception type thrown</param>
        /// <returns>Readable transaction exception message for customer</returns>
        private static string BuildErrorMsg(TransactionExceptionType exceptionType)
        {
            string expMsg = string.Empty;
            switch (exceptionType)
            {
                case TransactionExceptionType.InsufficentFund:
                    expMsg = string.Format("Insufficent fund, please check your balance.");
                    break;
                case TransactionExceptionType.SaveFailed:
                    expMsg = string.Format("Transaction failed, please try again later");
                    break;
                case TransactionExceptionType.TransferError:
                    expMsg = string.Format("Transfer error, please contact bank ASAP");
                    break;
                case TransactionExceptionType.InvalidTransferAccount:
                    expMsg = string.Format("Invalid transfer account, please try again.");
                    break;
                case TransactionExceptionType.TransferSameAccount:
                    expMsg = string.Format("Cannot transfer money to the same account.");
                    break;
                default:
                    expMsg = "Invalid transaction";
                    break;
            }
            return expMsg;
        }

        #endregion
    }
}
