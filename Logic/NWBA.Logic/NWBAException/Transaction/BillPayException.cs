﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NWBA.Logic.NWBAException.Transaction
{
    /// <summary>
    /// This class contain the custom message for the bill pay exception
    /// </summary>
    public class BillPayException : Exception
    {
        #region BillPay Exception Type
        public enum BillPayExceptionType
        {
            InvalidBillPay,
            SaveFailed,
            EnableFailed
        }

        #endregion


        #region Constructor

        public BillPayException(BillPayExceptionType exceptionType)
            : base(BuildErrorMsg(exceptionType))
        {

        }

        #endregion


        #region Private Static Methods

        /// <summary>
        /// This method build the custom message for the bill pay exception
        /// </summary>
        /// <param name="exceptionType">Bill pay exception type thrown</param>
        /// <returns>Readable bill pay exception message for customer</returns>
        private static string BuildErrorMsg(BillPayExceptionType exceptionType)
        {
            string expMsg = string.Empty;
            switch (exceptionType)
            {
                case BillPayExceptionType.InvalidBillPay:
                    expMsg = "Invalid Bill Pay Record";
                    break;
                case BillPayExceptionType.SaveFailed:
                    expMsg = string.Format("Bill Pay Saved error, please contact bank ASAP");
                    break;
                case BillPayExceptionType.EnableFailed:
                    expMsg = string.Format("Bill Pay enable error, you cannot enable cancelled bill pay");
                    break;
                default:
                    expMsg = "Bill Pay Error";
                    break;
            }
            return expMsg;
        }

        #endregion
    }
}
