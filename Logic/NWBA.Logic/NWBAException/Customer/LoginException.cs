﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NWBA.Logic.NWBAException.Customer
{
    /// <summary>
    /// This class contain the custom message for the login exception
    /// </summary>
    public class LoginException : Exception
    {
        #region Login Exception Type

        public enum LoginExceptionType
        {
            IncorrectPwd,
            InvalidUserNameAndCustomerID
        }

        #endregion


        #region Constructor

        public LoginException(LoginExceptionType exceptionType)
            : base(BuildErrorMsg(exceptionType))
        {

        }

        #endregion


        #region Private Static Methods

        /// <summary>
        /// This method build the custom message for the login exception
        /// </summary>
        /// <param name="exceptionType">Login exception type thrown</param>
        /// <returns>Readable login exception message for customer</returns>
        private static string BuildErrorMsg(LoginExceptionType exceptionType)
        {
            string expMsg = string.Empty;
            switch (exceptionType)
            {
                case LoginExceptionType.IncorrectPwd:
                    expMsg = "You have entered incorrect password";
                    break;
                case LoginExceptionType.InvalidUserNameAndCustomerID:
                    // This message is intent to confuse attacker for attempting to other customer's password
                    expMsg = "Invalid change password request";
                    break;
                default:
                    expMsg = "Login error";
                    break;
            }
            return expMsg;
        }

        #endregion
    }
}
