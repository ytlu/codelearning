﻿using System.Collections.Generic;
using System.Linq;
using NWBA.Common;
using NWBA.Data;
using NWBA.Domain.Interface;
using NWBA.Logic.Interface.Repository;
using Payee = NWBA.Domain.Entity.Payee;

namespace NWBA.Logic.Repository
{
    /// <summary>
    /// This class will handle the data related operation for Payee
    /// </summary>
    public class PayeeRepository : BaseRepository, IPayeeRepository
    {

        #region Public Methods

        /// <summary>
        /// Get the list of shared payee in the system
        /// </summary>
        /// <returns>List of payee</returns>
        public IList<IPayee> GetPayeeList()
        {
            using (var entities = new NWBADBEntities())
            {
                return entities.Payees
                    .Select(aa => aa).ToList()
                    .Select(aa => DataService.ConvertToObjectInterface<Data.Payee, Payee>(aa) as IPayee)
                    .ToList();
            }
        }

        #endregion

    }
}
