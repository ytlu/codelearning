﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NWBA.Common;
using NWBA.Data;
using NWBA.Domain.Entity;
using NWBA.Domain.Interface;
using NWBA.Logic.Interface.Repository;
using NWBA.Logic.NWBAException.Transaction;
using BillPay = NWBA.Domain.Entity.BillPay;

namespace NWBA.Logic.Repository
{
    /// <summary>
    /// This class will handle the data related operation for bill pay
    /// </summary>
    public class BillPayRepository : BaseRepository, IBillPayRepository
    {
        #region Bill Pay Status Enum

        private enum BillPayStatus
        {
            IsActive = 1,
            Cancelled = 2,
            Paused = 3
        }

        #endregion


        #region Public Methods

        /// <summary>
        /// Get the bill pay detail of a scheduled biller
        /// </summary>
        /// <param name="billPayID">Bill pay list</param>
        /// <returns>Account detail</returns>
        public IBillPay GetBillPay(int billPayID)
        {
            using (var entities = new NWBADBEntities())
            {
                return
                    DataService.ConvertToObjectInterface<Data.BillPay, BillPay>(
                        entities.BillPays.FirstOrDefault(aa => aa.BillPayID == billPayID));
            }
        }

        /// <summary>
        ///  Get bill pay list for the selected customer
        /// </summary>
        /// <param name="customerID">Customer ID to get the bill pay list</param>
        /// <param name="recordFrom">Record to be read from</param>
        /// <param name="numberOfItem">Number of records to return</param>
        /// <returns>List of bill pay belong to the customer</returns>
        public IList<ICustomerPayBill> GetCustomerBillPays(int customerID, int recordFrom, int numberOfItem)
        {
            using (var entities = new NWBADBEntities())
            {
                return entities.spGetBillPayList(customerID, recordFrom, numberOfItem)
                    .ToList()
                    .Select(
                        bb =>
                            DataService.ConvertToObjectInterface<Data.spGetBillPayList_Result, CustomerPayBill>(bb) as
                                ICustomerPayBill).ToList();
            }
        }

        /// <summary>
        ///  Get bill pay list for the all customers in the system
        /// </summary>
        /// <param name="recordFrom">Record to be read from</param>
        /// <param name="numberOfItem">Number of records to return</param>
        /// <returns>List of bill pay in the system</returns>
        public IList<IAdminBillPayRecord> GetAdminBillPays(int recordFrom, int numberOfItem)
        {
            using (var entities = new NWBADBEntities())
            {
                return entities.spGetAdminBillPayList(recordFrom, numberOfItem)
                    .ToList()
                    .Select(
                        bb =>
                            DataService.ConvertToObjectInterface<Data.spGetAdminBillPayList_Result, AdminBillPayRecord>(
                                bb) as IAdminBillPayRecord).ToList();
            }
        }

        /// <summary>
        /// Save the bill pay detail in bill pay table
        /// </summary>
        /// <param name="billPay">Bill pay details</param>
        /// <returns>Updated/created billpay number</returns>
        public int SaveBillPay(IBillPay billPay)
        {
            using (var entities = new NWBADBEntities())
            {
                var billPayDB = DataService.ConvertToObjectInterface<IBillPay, Data.BillPay>(billPay);

                if (billPayDB.BillPayID > 0)
                {
                    entities.BillPays.Attach(billPayDB);
                    entities.Entry(billPayDB).State = EntityState.Modified;
                }
                else
                {
                    entities.BillPays.Add(billPayDB);
                }

                billPayDB.Status = (int)BillPayStatus.IsActive;
                billPayDB.ModifyDate = DateTime.Now;

                entities.SaveChanges();

                return billPayDB.BillPayID;
            }
        }

        /// <summary>
        /// Cancel the bill pay so it will not included in the schedule payment as well as payment list
        /// The record cannot be deleted as there could be old payment related to this schedule payment.
        /// It should only be hidden from customer but kept for system reference
        /// </summary>
        /// <param name="billPayID">Bill pay ID</param>
        public void DeleteBillPay(int billPayID)
        {
            // Only hide the record, do not delete the record, because there could be
            // transaction data related to this schedule payment
            UpdateBillPayStatus(billPayID, BillPayStatus.Cancelled);
        }

        /// <summary>
        /// Enable the bill pay so the payment schedule can process the payment
        /// </summary>
        /// <param name="billPayID">Bill pay ID</param>
        public void EnableBillPay(int billPayID)
        {
            UpdateBillPayStatus(billPayID, BillPayStatus.IsActive);
        }

        /// <summary>
        /// Pause the bill pay so the payment schedule will skip this payment
        /// </summary>
        /// <param name="billPayID">Bill pay ID</param>
        public void PauseBillPay(int billPayID)
        {
            UpdateBillPayStatus(billPayID, BillPayStatus.Paused);
        }

        /// <summary>
        /// Get total number of bill pay belong to the customer
        /// </summary>
        /// <param name="customerID">Customer ID</param>
        /// <returns>Total number of bill pay belong to the customer</returns>
        public int TotalBillPayCount(int customerID)
        {
            using (var entities = new NWBADBEntities())
            {
                // Customer should only see active payment
                var activeStatus = (int)BillPayStatus.IsActive;
                return entities.BillPays.Count(bb => bb.Account.CustomerID == customerID && bb.Status == activeStatus);
            }
        }

        /// <summary>
        /// Get total number of bill pay in the system for all customer
        /// </summary>
        /// <param name="customerID">Customer ID</param>
        /// <returns>Total number of bill pay for the banking system</returns>
        public int TotalAdminBillPayCount()
        {
            using (var entities = new NWBADBEntities())
            {
                // Admin should see both paused and active payment
                var activeStatus = (int)BillPayStatus.IsActive;
                var pauseStatus = (int)BillPayStatus.Paused;
                return entities.BillPays.Count(bb => bb.Status == activeStatus || bb.Status == pauseStatus);
            }
        }

        /// <summary>
        /// Check if the bill pay record belong to the customer
        /// </summary>
        /// <param name="customerID">Customer ID</param>
        /// <param name="billPayID">Bill pay ID</param>
        /// <returns>True if the bill pay record belong to the customer</returns>
        public bool IsValidBillPayRecord(int customerID, int billPayID)
        {
            var hasMatchRecord = false;
            using (var entities = new NWBADBEntities())
            {
                hasMatchRecord =
                    entities.BillPays.Any(bb => bb.Account.CustomerID == customerID && bb.BillPayID == billPayID);
            }

            return hasMatchRecord;
        }

        #endregion


        #region Helper Methods

        /// <summary>
        /// This method pause/enable bill payment
        /// </summary>
        /// <param name="billPayID">Bill pay ID</param>
        /// <param name="newStatus">Status to be updated to</param>
        private void UpdateBillPayStatus(int billPayID, BillPayStatus newStatus)
        {
            using (var entities = new NWBADBEntities())
            {
                var billPayDB = entities.BillPays.First(bb => bb.BillPayID == billPayID);

                // Should not be allow admin to enable the payment cancelled by customer
                if (newStatus == BillPayStatus.IsActive && (billPayDB.Status == (int)BillPayStatus.Cancelled))
                {
                    throw new BillPayException(BillPayException.BillPayExceptionType.EnableFailed);
                }

                entities.BillPays.Attach(billPayDB);
                entities.Entry(billPayDB).State = EntityState.Modified;

                billPayDB.Status = (int)newStatus;
                billPayDB.ModifyDate = DateTime.Now;

                entities.SaveChanges();
            }
        }

        #endregion
    }
}