﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NWBA.Common;
using NWBA.Data;
using NWBA.Domain.Interface;
using NWBA.Logic.Interface.Repository;

namespace NWBA.Logic.Repository
{
    /// <summary>
    /// This class will handle the data related operation for transaction
    /// </summary>
    public class TransactionRepository : ITransactionRepository
    {

        #region Public Methods

        /// <summary>
        /// Save transaction details
        /// </summary>
        /// <param name="transaction">Transaction detail</param>
        /// <returns>Updated transaction ID</returns>
        public int SaveTransaction(ITransaction transaction)
        {
            using (var entities = new NWBADBEntities())
            {
                var transactionDB = DataService.ConvertToObjectInterface<ITransaction, Data.Transaction>(transaction);

                if (transactionDB.TransactionID > 0)
                {
                    entities.Transactions.Attach(transactionDB);
                    entities.Entry(transactionDB).State = EntityState.Modified;
                }
                else
                {
                    entities.Transactions.Add(transactionDB);
                }

                entities.SaveChanges();

                return transactionDB.TransactionID;
            }
        }

        /// <summary>
        /// Get number of chargable transaction for a specific account
        /// </summary>
        /// <param name="accountNumber">Account number</param>
        /// <returns>Number of chargable transaction for an account</returns>
        public int GetChargableTransactionCount(int accountNumber)
        {
            using (var entities = new NWBADBEntities())
            {
                /* This function will be calling store proc to improve performance                 
                 * This store proc will select the records which fits the following conditions:
                 * - Debit type of transaction (excluded transfer)
                 * - Transfer type which transfer the money out (the destinated account should be have third charge)
                 
                 	SELECT @chargableTransaction = Count(TransactionID) 
	                FROM   [Transaction] 
	                WHERE  AccountNumber = @AccountNumber
		                   AND ( TransactionType IN ( 'W', 'B' ) 
				                  OR ( TransactionType = 'T' AND Amount < 0 ) )

	                SELECT ISNULL(@chargableTransaction, 0) AS [ChargableTransaction]
                 */
                return entities.spGetChargableTransaction(accountNumber).FirstOrDefault().Value;
            }
        }

        #endregion

    }
}
