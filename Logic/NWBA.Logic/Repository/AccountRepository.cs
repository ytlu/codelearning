﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NWBA.Common;
using NWBA.Data;
using NWBA.Domain.Interface;
using NWBA.Logic.Interface.Repository;
using Account = NWBA.Domain.Entity.Account;
using Transaction = NWBA.Domain.Entity.Transaction;

namespace NWBA.Logic.Repository
{
    /// <summary>
    /// This class will handle the data related operation for account
    /// </summary>
    public class AccountRepository : BaseRepository, IAccountRepository
    {
        #region Public Methods

        /// <summary>
        /// Get list of accounts belong to customer
        /// </summary>
        /// <param name="customerID">Customer ID of the customer</param>
        /// <returns>List of account belong to the customer</returns>
        public IList<IAccount> GetCustomerAccounts(int customerID)
        {
            using (var entities = new NWBADBEntities())
            {
                return entities.Accounts
                    .Where(aa => aa.CustomerID == customerID)
                    .Select(aa => aa).ToList()
                    .Select(aa => DataService.ConvertToObjectInterface<Data.Account, Account>(aa) as IAccount)
                    .ToList();
            }
        }

        /// <summary>
        /// Get the account detail of a selected account
        /// </summary>
        /// <param name="accountNumber">Account number</param>
        /// <returns>Account detail</returns>
        public IAccount GetAccount(int accountNumber)
        {
            using (var entities = new NWBADBEntities())
            {
                return
                    DataService.ConvertToObjectInterface<Data.Account, Account>(
                        entities.Accounts.FirstOrDefault(aa => aa.AccountNumber == accountNumber));
            }
        }

        /// <summary>
        /// Save the account detail against an account
        /// </summary>
        /// <param name="account">Account details</param>
        /// <returns>Updated account number</returns>
        public int SaveAccount(IAccount account)
        {
            using (var entities = new NWBADBEntities())
            {
                // Convert from domain model to EF model
                var accountDB = DataService.ConvertToObjectInterface<IAccount, Data.Account>(account);

                if (accountDB.AccountNumber > 0)
                {
                    entities.Accounts.Attach(accountDB);
                    entities.Entry(accountDB).State = EntityState.Modified;
                }
                else
                {
                    entities.Accounts.Add(accountDB);
                }

                entities.SaveChanges();

                return accountDB.AccountNumber;
            }
        }

        /// <summary>
        ///  Get account transactions for the selected account
        /// </summary>
        /// <param name="accountNumber">Account number</param>
        /// <param name="recordFrom">Record to be read from</param>
        /// <param name="numberOfItem">Number of records to return</param>
        /// <returns>List of transaction which is sorted by created date descending</returns>
        public IList<ITransaction> GetAccountTransactions(int accountNumber, int recordFrom, int numberOfItem)
        {
            using (var entities = new NWBADBEntities())
            {
                return entities.Transactions
                    .Where(tt => tt.AccountNumber == accountNumber)
                    .OrderByDescending(tt => tt.ModifyDate)
                    .Skip(recordFrom)
                    .Take(numberOfItem)
                    .ToList()
                    .Select(
                        tt => DataService.ConvertToObjectInterface<Data.Transaction, Transaction>(tt) as ITransaction)
                    .ToList();
            }
        }

        /// <summary>
        /// Get number of transactions for the selected account
        /// </summary>
        /// <param name="accountNumber">Account number</param>
        /// <returns>Count of the transaction which belong to the selected account</returns>
        public int TotalTransaction(int accountNumber)
        {
            using (var entities = new NWBADBEntities())
            {
                return entities.Transactions.Count(transaction => transaction.AccountNumber == accountNumber);
            }
        }

        #endregion
    }
}