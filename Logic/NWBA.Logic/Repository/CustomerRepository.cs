﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using NWBA.Common;
using NWBA.Data;
using NWBA.Domain.Interface;
using NWBA.Logic.Interface.Repository;
using NWBA.Logic.NWBAException.Customer;
using Customer = NWBA.Domain.Entity.Customer;

namespace NWBA.Logic.Repository
{
    /// <summary>
    /// This class will handle the data related operation for customer
    /// </summary>
    public class CustomerRepository : BaseRepository, ICustomerRepository
    {

        #region Public Methods

        /// <summary>
        /// Get customer detail by customer ID
        /// </summary>
        /// <param name="customerID">Customer ID</param>
        /// <returns>Customer detail</returns>
        public ICustomer GetCustomerByID(int customerID)
        {
            using (var entities = new NWBADBEntities())
            {
                return DataService.ConvertToObjectInterface<Data.Customer, Customer>(entities.Customers.FirstOrDefault(cc => cc.CustomerID == customerID));
            }
        }

        /// <summary>
        /// Get customer detail by username, which is stored against Login table
        /// </summary>
        /// <param name="userName">Username of customer</param>
        /// <returns>Customer detail</returns>
        public ICustomer GetCustomerByUserName(string userName)
        {
            using (var entities = new NWBADBEntities())
            {
                return DataService.ConvertToObjectInterface<Data.Customer, Customer>(entities.Customers.FirstOrDefault(cc => cc.Login.UserID == userName));
            }
        }

        /// <summary>
        /// Get customer ID by username, this is reduce data transfer across the network
        /// </summary>
        /// <param name="userName">Username of customer</param>
        /// <returns></returns>
        public int GetCustomerIDByUserName(string userName)
        {
            using (var entities = new NWBADBEntities())
            {
                return entities.Customers.FirstOrDefault(cc => cc.Login.UserID == userName).CustomerID;
            }
        }

        /// <summary>
        /// Get list of account belong to the customer
        /// </summary>
        /// <param name="customerID">Customer ID</param>
        /// <returns>List of account belong to the customer</returns>
        public IList<int> GetCustomerAccountList(int customerID)
        {
            using (var entities = new NWBADBEntities())
            {
                return entities.Accounts.Where(acc => acc.CustomerID == customerID).Select(acc => acc.AccountNumber).ToList();
            }
        }

        /// <summary>
        /// Get customers list in the system
        /// </summary>
        /// <param name="recordFrom">Record to be read from</param>
        /// <param name="numberOfItem">Number of records to return</param>
        /// <returns>List of paged customers</returns>
        public IList<ICustomer> GetCustomers(int recordFrom, int numberOfItem)
        {
            using (var entities = new NWBADBEntities())
            {
                return entities.Customers
                    .OrderBy(cc => cc.CustomerID)
                    .Skip(recordFrom)
                    .Take(numberOfItem)
                    .ToList()
                    .Select(aa => DataService.ConvertToObjectInterface<Data.Customer, Customer>(aa) as ICustomer)
                    .ToList();
            }
        }

        /// <summary>
        /// Update customer detail only
        /// Attempt to create customer using this method will throw exception
        /// </summary>
        /// <param name="customer">Customer detail to be updated</param>
        /// <returns>Updated Customer ID</returns>
        public int SaveCustomer(ICustomer customer)
        {
            using (var entities = new NWBADBEntities())
            {
                if (customer.CustomerID > 0)
                {
                    var customerDB = entities.Customers.FirstOrDefault(cc => cc.CustomerID == customer.CustomerID);

                    // Manually map name and TFN, we do not want user to be able to change login ID and address ID
                    customerDB.CustomerName = customer.CustomerName;
                    customerDB.TFN = customer.TFN;

                    entities.Customers.Attach(customerDB);
                    entities.Entry(customerDB).State = EntityState.Modified;

                    entities.SaveChanges();
                    return customerDB.CustomerID;
                }
                else
                {
                    throw new Exception("Please contact bank to create a new account");
                }
            }
        }

        /// <summary>
        /// Verify if the customer ID and username match with database record
        /// If no record found, an exception will be thrown.
        /// </summary>
        /// <param name="customerID">Customer ID</param>
        /// <param name="userName">Username</param>
        public void VerifyLoginByCustomerIDandUserName(int customerID, string userName)
        {
            using (var entites = new NWBADBEntities())
            {
                if (!entites.Customers.Any(cc => cc.CustomerID == customerID && cc.Login.UserID == userName))
                {
                    throw new LoginException(LoginException.LoginExceptionType.InvalidUserNameAndCustomerID);
                }
            }
        }

        /// <summary>
        /// Get total count of customer in the system
        /// </summary>
        /// <returns>Total count of the customers</returns>
        public int TotalCustomerCount()
        {
            using (var entities = new NWBADBEntities())
            {
                return entities.Customers.Count();
            }
        }

        #endregion

    }
}
