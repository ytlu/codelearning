﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NWBA.Logic.Utils;

namespace NWBA.Logic.Repository
{
    /// <summary>
    /// This is base repository where all repository should inherit from
    /// This class ensure the object mapping is setup
    /// </summary>
    public abstract class BaseRepository
    {
        #region Protected Constructor

        /// <summary>
        /// Protected constructor which ensure the object mapping been called
        /// </summary>
        protected BaseRepository()
        {
            DataTransformer.Instance.EnsureMapping();
        }

        #endregion
    }
}
