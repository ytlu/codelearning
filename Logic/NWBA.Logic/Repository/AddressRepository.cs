﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NWBA.Common;
using NWBA.Data;
using NWBA.Domain.Interface;
using NWBA.Logic.Interface.Repository;
using Address = NWBA.Domain.Entity.Address;

namespace NWBA.Logic.Repository
{
    /// <summary>
    /// This class will handle the data related operation for address
    /// </summary>
    public class AddressRepository : BaseRepository, IAddressRepository
    {
        #region Public Methods

        /// <summary>
        /// Get address detail by address ID
        /// </summary>
        /// <param name="addressID">Address ID</param>
        /// <returns>Address detail</returns>
        public IAddress GetAddressByID(int addressID)
        {
            using (var entities = new NWBADBEntities())
            {
                return
                    DataService.ConvertToObjectInterface<Data.Address, Address>(
                        entities.Addresses.FirstOrDefault(aa => aa.AddressID == addressID));
            }
        }

        /// <summary>
        /// Get address detail below to a customer
        /// </summary>
        /// <param name="customerID">Customer ID</param>
        /// <returns>Address detail</returns>
        public IAddress GetAddressByCustomerID(int customerID)
        {
            using (var entities = new NWBADBEntities())
            {
                return
                    DataService.ConvertToObjectInterface<Data.Address, Address>(
                        entities.Customers.FirstOrDefault(cc => cc.CustomerID == customerID).Address);
            }
        }

        /// <summary>
        /// Save address to the database
        /// </summary>
        /// <param name="address">Address detail to be saved</param>
        /// <returns>Updated address ID</returns>
        public int SaveAddress(IAddress address)
        {
            using (var entities = new NWBADBEntities())
            {
                var addressDB = DataService.ConvertToObjectInterface<IAddress, Data.Address>(address);

                if (addressDB.AddressID > 0)
                {
                    entities.Addresses.Attach(addressDB);
                    entities.Entry(addressDB).State = EntityState.Modified;
                }
                else
                {
                    entities.Addresses.Add(addressDB);
                }

                entities.SaveChanges();

                return addressDB.AddressID;
            }
        }

        #endregion
    }
}