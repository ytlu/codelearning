﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NWBA.Common;
using NWBA.Data;
using NWBA.Domain.Interface;
using NWBA.Logic.Interface.Repository;
using Login = NWBA.Domain.Entity.Login;

namespace NWBA.Logic.Repository
{
    /// <summary>
    /// This class will handle the data related operation for login
    /// </summary>
    public class LoginRepository : BaseRepository, ILoginRepository
    {
        #region Public Methods

        /// <summary>
        /// Verify if the username and password match with user record
        /// </summary>
        /// <param name="userName">Username</param>
        /// <param name="password">Password</param>
        /// <returns>True if the username and password match with user record</returns>
        public bool IsValidLogin(string userName, string password)
        {
            using (var entities = new NWBADBEntities())
            {
                return entities.Logins.Any(login => login.UserID == userName && login.Password == password);
            }
        }

        /// <summary>
        /// Verify if the user is an administrator
        /// </summary>
        /// <param name="userName">Username</param>
        /// <returns>True if the user is an administrator</returns>
        public bool IsAdministrator(string userName)
        {
            using (var entities = new NWBADBEntities())
            {
                return entities.Logins.Any(login => login.UserID == userName && login.IsAdmin);
            }
        }

        /// <summary>
        /// Get login detail by username
        /// </summary>
        /// <param name="userName">Username of the login</param>
        /// <returns>Login detail</returns>
        public ILogin GetLoginByUserName(string userName)
        {
            using (var entities = new NWBADBEntities())
            {
                return DataService.ConvertToObjectInterface<Data.Login, Login>(
                    entities.Logins.FirstOrDefault(ll => ll.UserID == userName));
            }
        }

        /// <summary>
        /// Update password for a login
        /// </summary>
        /// <param name="loginID">Login ID</param>
        /// <param name="newPassword">New password</param>
        public void UpdatePassword(int loginID, string newPassword)
        {
            using (var entities = new NWBADBEntities())
            {
                var loginDB = entities.Logins.FirstOrDefault(ll => ll.LoginID == loginID);
                loginDB.Password = newPassword;
                loginDB.ModifyDate = DateTime.Now;

                entities.Logins.Attach(loginDB);
                entities.Entry(loginDB).State = EntityState.Modified;

                entities.SaveChanges();
            }
        }

        #endregion
    }
}
