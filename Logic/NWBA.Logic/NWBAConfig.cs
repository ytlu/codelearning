﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NWBA.Logic
{
    /// <summary>
    /// This class contains all the configuration value for logic layer
    /// Any configuration should be manage in this class ONLY.
    /// </summary>
    public class NWBAConfig
    {
        /// <summary>
        /// Encrypt key value for the password
        /// </summary>
        public static string  ENCRY_KEY_VALUE = "NWBA_Assignment_Two";

        /// <summary>
        /// Maximum free transaction allowed per account
        /// </summary>
        internal static int MAX_FREE_TRANSACTION_ALLOWED = 4;
    }
}
