﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NWBA.Common;
using NWBA.Logic.Interface.Repository;
using NWBA.Logic.Interface.Service;
using NWBA.Logic.NWBAException.Customer;

namespace NWBA.Logic.Service
{
    /// <summary>
    /// This class is use for login related operation such as validate login, update password, etc.
    /// </summary>
    public class LoginService : BaseService, ILoginService
    {
        #region Private Members

        private readonly ILoginRepository _loginRepository;

        #endregion


        #region Constructor

        public LoginService(ILoginRepository loginRepository)
            : base(typeof(LoginService))
        {
            _loginRepository = loginRepository;
        }

        #endregion


        #region Pubilc Methods

        /// <summary>
        /// Validate username and password
        /// </summary>
        /// <param name="userName">Username</param>
        /// <param name="password">Password</param>
        /// <returns>True if username match with password, false otherwise</returns>
        public bool IsValidLogin(string userName, string password)
        {
            GetLogger.Debug("Calling::IsValidLogin");
            GetLogger.DebugFormat("Attempt to validate user: {0}", userName);

            // The password is one way encrypted
            return _loginRepository.IsValidLogin(userName, CipherService.Encrypt(password, NWBAConfig.ENCRY_KEY_VALUE));
        }

        /// <summary>
        /// Verify if the user is an administrator
        /// </summary>
        /// <param name="userName">Username</param>
        /// <returns>True if the user is an administrator</returns>
        public bool IsAdministrator(string userName)
        {
            return _loginRepository.IsAdministrator(userName);
        }

        /// <summary>
        /// Update password for a login
        /// </summary>
        /// <param name="userName">Username for the login</param>
        /// <param name="currentPassword">Current password of the user</param>
        /// <param name="newPassword">New password for the user</param>
        public void UpdatePassword(string userName, string currentPassword, string newPassword)
        {
            GetLogger.Debug("Calling::UpdatePassword");
            GetLogger.DebugFormat("Attempt to update password for: {0}", userName);

            var login = _loginRepository.GetLoginByUserName(userName);

            var pwd = CipherService.Encrypt(currentPassword, NWBAConfig.ENCRY_KEY_VALUE);
            var newPwd = CipherService.Encrypt(newPassword, NWBAConfig.ENCRY_KEY_VALUE);

            // Make sure username and password match before proceed with update the record
            if (login.Password != pwd)
            {
                GetLogger.ErrorFormat("User: {0}, enter incorrect pwd", userName);
                throw new LoginException(LoginException.LoginExceptionType.IncorrectPwd);
            }

            _loginRepository.UpdatePassword(login.LoginID, newPwd);
        }

        #endregion

    }
}
