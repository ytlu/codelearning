﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NWBA.Domain.Entity;
using NWBA.Domain.Interface;
using NWBA.Logic.Interface.Repository;
using NWBA.Logic.Interface.Service;
using NWBA.Logic.NWBAException.Transaction;
using NWBA.Logic.Repository;
using NWBA.Logic.Utils;

namespace NWBA.Logic.Service
{
    /// <summary>
    /// This class manage the transaction related operation.
    /// Transactions made should be using this class to ensure all the business logic is applied correctly
    /// </summary>
    public class TransactionService : BaseService, ITransactionService
    {
        #region Private Members

        private readonly ITransactionRepository _transactionRepository;

        #endregion


        #region Constructor

        public TransactionService(ITransactionRepository transactionRepository)
            : base(typeof(TransactionService))
        {
            _transactionRepository = transactionRepository;
        }

        #endregion


        #region Public Methods

        /// <summary>
        /// This method will save the transaction against an account. It will also applied and third charge and return the new balance for the account
        /// </summary>
        /// <param name="transaction">Transaction detail</param>
        /// <param name="account">Account to be transact against</param>
        /// <param name="allowedBalance">Account type minimum balance</param>
        /// <param name="newBalance">New balance for the account</param>
        /// <returns>Transaction ID</returns>
        public int SaveTransaction(ITransaction transaction, IAccount account, out decimal newBalance)
        {
            GetLogger.Debug("Calling::SaveTransaction");

            // Get surcharge detail
            var surcharge = NWBABusinessRule.TransactionRule.GetSurChargeAmount(transaction.TransactionType);

            // If transaction attract surcharge, check if it's still within free transaction
            if (surcharge > 0)
            {
                GetLogger.DebugFormat("Transaction required surcharge charge, amount: {0}", surcharge);

                var totalChargableTransaction =
                    _transactionRepository.GetChargableTransactionCount(account.AccountNumber);

                // If transaction still within free transaction, clear third charge
                if (totalChargableTransaction < NWBABusinessRule.AccountRule.MaxFreeTransactionAllowed)
                {
                    GetLogger.Debug("Transaction still within free transaction limit");
                    surcharge = 0;
                }
            }

            // Validate account balance before proceed
            VerifyTransaction(transaction, account.AccountType, account.Balance, surcharge);

            // Check if the transaction is credit or debit
            if (!NWBABusinessRule.TransactionRule.IsCreditTransaction(transaction.TransactionType))
            {
                GetLogger.Debug("Transaction is credit");
                transaction.Amount *= -1;
            }

            transaction.ModifyDate = DateTime.Now;
            transaction.DestAccount = (transaction.DestAccount == 0 ? null : transaction.DestAccount);

            // Save the transaction against the account
            int transactionID = _transactionRepository.SaveTransaction(transaction);

            if (transactionID > 0)
            {
                // Apply third charge
                if (surcharge > 0)
                {
                    ITransaction surchargeTrans = new Transaction()
                    {
                        AccountNumber = account.AccountNumber,
                        DestAccount = null,
                        Amount = surcharge * -1,
                        Comment =
                            NWBABusinessRule.TransactionRule.GetThirdChargeDescription(transaction.TransactionType),
                        TransactionType = "S",
                        ModifyDate = DateTime.Now
                    };

                    GetLogger.DebugFormat("Surcharge transaction save sucessfully, amount: {0}", surcharge);

                    _transactionRepository.SaveTransaction(surchargeTrans);
                }

                newBalance = (account.Balance + transaction.Amount - surcharge);

                GetLogger.DebugFormat("New balance is: {0}", newBalance);

                return transactionID;
            }
            else
            {
                GetLogger.ErrorFormat("Error saving tranaction");
                throw new TransactionException(TransactionException.TransactionExceptionType.SaveFailed);
            }
        }

        /// <summary>
        /// This method transfer the fund into other account
        /// </summary>
        /// <param name="transaction">Transaction detail</param>
        /// <param name="account">Destination account where fund goes to</param>
        /// <returns>The new balance of the destinated account</returns>
        public decimal TransferFund(ITransaction transaction, IAccount account)
        {
            // Setup a transaction to be saved
            ITransaction transferTrans = new Transaction()
            {
                AccountNumber = transaction.DestAccount.Value,
                DestAccount = transaction.AccountNumber,
                Amount = transaction.Amount * -1,
                Comment = transaction.Comment,
                TransactionType = "T",
                ModifyDate = DateTime.Now
            };

            // Save the transaction against the account
            int transactionID = _transactionRepository.SaveTransaction(transferTrans);

            if (transactionID > 0)
            {
                return account.Balance + transferTrans.Amount;
            }
            else
            {
                throw new TransactionException(TransactionException.TransactionExceptionType.TransferError);
            }
        }

        #endregion


        #region Helper Methods

        /// <summary>
        /// Verify if the transaction is valid before proceed
        /// </summary>
        /// <param name="transaction">Transaction detail</param>
        /// <param name="accountType">Account type for the transaction</param>
        /// <param name="accountBalance">Current account balance</param>
        /// <param name="thirdCharge">The third charge amount</param>
        private void VerifyTransaction(ITransaction transaction, string accountType, decimal accountBalance,
            decimal thirdCharge)
        {
            if (NWBABusinessRule.TransactionRule.IsCreditTransaction(transaction.TransactionType))
            {
                accountBalance += transaction.Amount - thirdCharge;
            }
            else
            {
                accountBalance -= transaction.Amount - thirdCharge;
            }

            // Ensure the end result balance still met the minimum balance required by NWBA
            if (accountBalance < NWBABusinessRule.AccountRule.MinimumBalanceAllowed(accountType))
                throw new TransactionException(TransactionException.TransactionExceptionType.InsufficentFund);
        }

        #endregion
    }
}