﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Handlers;
using System.Web.UI;
using NWBA.Domain.Entity;
using NWBA.Domain.Interface;
using NWBA.Logic.Interface.Repository;
using NWBA.Logic.Interface.Service;
using NWBA.Logic.NWBAException.Transaction;
using NWBA.Logic.Utils;

namespace NWBA.Logic.Service
{
    /// <summary>
    /// This class manage account related operation.
    /// </summary>
    public class AccountService : BaseService, IAccountService
    {
        #region Private Members

        private readonly IAccountRepository _accountRepository;
        private readonly ITransactionService _transactionService;
        private readonly IPagingService _pagingService;

        #endregion


        #region Constructor

        public AccountService(IAccountRepository accountRepository, ITransactionService transactionService,
            IPagingService pagingService)
            : base(typeof(AccountService))
        {
            _accountRepository = accountRepository;
            _transactionService = transactionService;
            _pagingService = pagingService;
        }

        #endregion


        #region Public Methods

        /// <summary>
        /// Get account detail via account number
        /// </summary>
        /// <param name="accountNumber">Customer account number</param>
        /// <returns>Account detail</returns>
        public IAccount GetAccountDetail(int accountNumber)
        {
            return _accountRepository.GetAccount(accountNumber);
        }

        /// <summary>
        /// Get list of the account of a customer
        /// </summary>
        /// <param name="customerID">Customer ID of customer</param>
        /// <returns>List of accounts belong to the customer</returns>
        public IList<IAccount> GetCustomerAccounts(int customerID)
        {
            return _accountRepository.GetCustomerAccounts(customerID);
        }

        /// <summary>
        /// Get list of transaction for a particulare account number
        /// </summary>
        /// <param name="accountNumber">Account number</param>
        /// <param name="selectedPage">Select page for the transaction records</param>
        /// <returns>Account transaction detail for the account</returns>
        public IAccountTransaction GetAccountTransactions(int accountNumber, int selectedPage)
        {
            var accountTransaction = new AccountTransaction()
            {
                HasTransaction = false
            };

            var totalRecords = TotalTransaction(accountNumber);

            // Only try to retrive the transaction records if customer have account
            if (totalRecords > 0)
            {
                accountTransaction.HasTransaction = true;

                // Populate the paging model
                accountTransaction.PageModel = GetTransactionPageModel(totalRecords, selectedPage);

                // Get the records according to selected page
                var paging = _pagingService.GetRecordRange(selectedPage);

                // Retrieve the transaction records for the account with paging
                accountTransaction.Transactions =
                    _accountRepository.GetAccountTransactions(
                        accountNumber,
                        paging.RecordFrom, paging.NumberOfItem);
            }

            return accountTransaction;
        }

        /// <summary>
        /// Save the transaction against the selected account
        /// </summary>
        /// <param name="transaction">Transaction to be saved</param>
        /// <returns>Transaction saved result</returns>
        public ITransactionResult SaveTransaction(ITransaction transaction)
        {
            GetLogger.Debug("Calling::SaveTransaction");

            GetLogger.DebugFormat("Attempt to load account: #{0}", transaction.AccountNumber);
            // Load the account detail
            var account = GetAccountDetail(transaction.AccountNumber);

            // Create the transaction result
            var transactionResult = new TransactionResult
            {
                AccountNumber = transaction.AccountNumber,
                TransactionTypeDesc = DataTransformer.MapTransactionTypeToDesc(transaction.TransactionType),
                DestAccountNumber = transaction.DestAccount,
                TransactionDate = DateTime.Now,
                Amount = transaction.Amount,
                Comment = transaction.Comment
            };

            IAccount destAcc = null;
            bool isTransfer = false;

            // If transaction is transfer, make sure to account is valid
            if (transaction.TransactionType == "T")
            {
                GetLogger.Debug("Making transfer");

                // Make sure customer selected an account to transfer to
                if (!transaction.DestAccount.HasValue)
                {
                    throw new TransactionException(TransactionException.TransactionExceptionType.InvalidTransferAccount);
                }

                // Make sure customer did not transfer to same account
                if (transaction.AccountNumber == transaction.DestAccount.Value)
                {
                    throw new TransactionException(TransactionException.TransactionExceptionType.TransferSameAccount);
                }

                destAcc = _accountRepository.GetAccount(transaction.DestAccount.Value);

                // Make sure the TO account exists
                if (destAcc != null && destAcc.AccountNumber == 0)
                {
                    GetLogger.ErrorFormat("Invalid transfer account, dest account: #{0}", transaction.DestAccount.Value);
                    throw new TransactionException(TransactionException.TransactionExceptionType.InvalidTransferAccount);
                }

                isTransfer = true;

            }

            // Save transaction and get updated balance
            decimal newBalance = 0;
            transactionResult.ReceiptNumber = _transactionService.SaveTransaction(transaction, account, out newBalance);
            transactionResult.AccountBalance = newBalance;
            account.Balance = newBalance;
            account.ModifyDate = transactionResult.TransactionDate;
            _accountRepository.SaveAccount(account);

            // Transfer money to destinated account
            if (isTransfer)
            {
                DateTime transferDate = DateTime.Now;
                GetLogger.Debug("Adjust destination account balance");
                destAcc.Balance = _transactionService.TransferFund(transaction, destAcc);

                GetLogger.DebugFormat("Transfer detail, acc: #{0}, balance: {1}, on date: {2}",
                    destAcc.AccountNumber, destAcc.Balance, transferDate);

                destAcc.ModifyDate = transferDate;
                _accountRepository.SaveAccount(destAcc);
            }

            return transactionResult;
        }

        #endregion


        #region Helper Methods

        /// <summary>
        /// Build paging model for the paging control
        /// </summary>
        /// <param name="totalRecords">Total records</param>
        /// <param name="selectedPage">Selected ppage</param>
        /// <returns>Paging model for paging control display</returns>
        private IPageModel GetTransactionPageModel(int totalRecords, int selectedPage)
        {
            return _pagingService.BuildPageModel(totalRecords, selectedPage);
        }

        /// <summary>
        /// Get the total transaction count for an account
        /// </summary>
        /// <param name="accountNumber">Account number</param>
        /// <returns>Total number of transaction for an account</returns>
        private int TotalTransaction(int accountNumber)
        {
            return _accountRepository.TotalTransaction(accountNumber);
        }

        #endregion

    }
}