﻿using System.Collections.Generic;
using NWBA.Domain.Entity;
using NWBA.Domain.Interface;
using NWBA.Logic.Interface.Repository;
using NWBA.Logic.Interface.Service;
using NWBA.Logic.NWBAException.Transaction;

namespace NWBA.Logic.Service
{
    /// <summary>
    /// This class manage bill pay operation.
    /// </summary>
    public class BillPayService : BaseService, IBillPayService
    {
        #region Private Members

        private readonly IBillPayRepository _billPayRepository;
        private readonly IPagingService _pagingService;

        #endregion


        #region Constructor

        public BillPayService(IBillPayRepository billPayRepository, IPagingService pagingService)
            : base(typeof(BillPayService))
        {
            _billPayRepository = billPayRepository;
            _pagingService = pagingService;
        }

        #endregion


        #region Public Methods

        /// <summary>
        /// Get list of bill pay for a particulare customer
        /// </summary>
        /// <param name="customerID">Customer ID</param>
        /// <param name="selectedPage">Select page for the bill pay records</param>
        /// <returns>Bill pay list for the customer</returns>
        public IBillPayRecord GetBillPays(int customerID, int selectedPage)
        {
            var billRecords = new BillPayRecord()
            {
                HasRecord = false
            };

            var totalRecords = TotalBillPayCounts(customerID);

            // Only try to retrive the bill pay records if bill pay record exists
            if (totalRecords > 0)
            {
                billRecords.HasRecord = true;

                // Populate the paging model
                billRecords.PageModel = GetRecordPageMode(totalRecords, selectedPage);

                // Get the records according to selected page
                var paging = _pagingService.GetRecordRange(selectedPage);

                // Retrieve the bill pay records for the customer with paging
                billRecords.BillPays =
                    _billPayRepository.GetCustomerBillPays(customerID, paging.RecordFrom, paging.NumberOfItem);
            }

            return billRecords;
        }

        /// <summary>
        /// Get list of bill pay in the system
        /// </summary>
        /// <param name="selectedPage">Select page for the bill pay records</param>
        /// <returns>Bill pay list for selected page</returns>
        public IAdminSchedulePaymentModel GetAdminBillPays(int selectedPage)
        {
            var billRecords = new AdminSchedulePaymentModel()
            {
                HasBillPay = false
            };

            var totalRecords = TotalAdminBillPayCounts();

            // Only try to retrive the bill pay records if bill pay record exists
            if (totalRecords > 0)
            {
                billRecords.HasBillPay = true;

                // Populate the paging model
                billRecords.PageModel = GetRecordPageMode(totalRecords, selectedPage);

                // Get the records according to selected page
                var paging = _pagingService.GetRecordRange(selectedPage);

                // Retrieve the bill pay records with paging
                billRecords.BillPayRecords =
                    _billPayRepository.GetAdminBillPays(paging.RecordFrom, paging.NumberOfItem);
            }

            return billRecords;
        }

        /// <summary>
        /// Get bill pay detail
        /// </summary>
        /// <param name="customerID">Customer ID</param>
        /// <param name="billPayID">Selected bill pay records</param>
        /// <returns>Bill pay detail</returns>
        public IBillPay GetBillPay(int customerID, int billPayID)
        {
            // Verify if the bill pay record belong to the customer first
            if (!_billPayRepository.IsValidBillPayRecord(customerID, billPayID))
            {
                throw new BillPayException(BillPayException.BillPayExceptionType.InvalidBillPay);
            }

            return _billPayRepository.GetBillPay(billPayID);
        }

        /// <summary>
        /// Save bill pay record
        /// </summary>
        /// <param name="billPay">Bill pay detail to be saved</param>
        /// <returns>The created/updated bill pay record ID</returns>
        public int SaveBillPay(IBillPay billPay)
        {
            GetLogger.Debug("Calling::SaveBillPay");
            GetLogger.DebugFormat("Bill to save, from: #{0}, to: #{1}, amount: {2}",
                billPay.AccountNumber,
                billPay.PayeeID,
                billPay.Amount);

            // Make the schedule time to the end of day instead of beginning, the scheduler should run at the end of the day
            billPay.ScheduleDate = billPay.ScheduleDate.AddDays(1).AddSeconds(-1);

            int savedBillPayID = _billPayRepository.SaveBillPay(billPay);

            // Create/Update operation failed, throw exception to be manage by the website
            if (savedBillPayID < 0)
            {
                GetLogger.Error("Failed to save payment");
                throw new BillPayException(BillPayException.BillPayExceptionType.SaveFailed);
            }

            return savedBillPayID;
        }

        /// <summary>
        /// Cancel the bill pay so it will not included in the schedule payment as well as payment list
        /// </summary>
        /// <param name="customerID">Customer ID</param>
        /// <param name="billPayID">Bill pay ID</param>
        public void DeleteBillPay(int customerID, int billPayID)
        {
            GetLogger.Debug("Calling::DeleteBillPay");

            // Verify if the record belong to customer before delete it
            if (!_billPayRepository.IsValidBillPayRecord(customerID, billPayID))
            {
                GetLogger.ErrorFormat("Invalid bill payment, customer: #{0}, bill pay ID: {1}",
                    customerID, billPayID);
                throw new BillPayException(BillPayException.BillPayExceptionType.InvalidBillPay);
            }

            _billPayRepository.DeleteBillPay(billPayID);

        }

        /// <summary>
        /// Enable the bill pay so the payment schedule can process the payment
        /// </summary>
        /// <param name="billPayID">Bill pay ID</param>
        public void EnableBillPay(int billPayID)
        {
            GetLogger.Debug("Calling::EnableBillPay");
            GetLogger.DebugFormat("Billing pay id to enable: {0}", billPayID);
            _billPayRepository.EnableBillPay(billPayID);
        }

        /// <summary>
        /// Pause the bill pay so the payment schedule will skip this payment
        /// </summary>
        /// <param name="billPayID">Bill pay ID</param>
        public void PauseBillPay(int billPayID)
        {
            GetLogger.Debug("Calling::PauseBillPay");
            GetLogger.DebugFormat("Billing pay id to pause: {0}", billPayID);
            _billPayRepository.PauseBillPay(billPayID);
        }

        #endregion


        #region Helper Methods

        /// <summary>
        /// Build paging model for the paging control
        /// </summary>
        /// <param name="totalRecords">Total records</param>
        /// <param name="selectedPage">Selected ppage</param>
        /// <returns>Paging model for paging control display</returns>
        private IPageModel GetRecordPageMode(int totalRecords, int selectedPage)
        {
            return _pagingService.BuildPageModel(totalRecords, selectedPage);
        }

        /// <summary>
        /// Get the total bill pay count for a customer
        /// </summary>
        /// <param name="customerID">Customer ID</param>
        /// <returns>Total number of bill pay for a customer</returns>
        private int TotalBillPayCounts(int customerID)
        {
            return _billPayRepository.TotalBillPayCount(customerID);
        }

        /// <summary>
        /// Get the total bill pay in the system
        /// </summary>
        /// <returns>Total number of bill pay in the system</returns>
        private int TotalAdminBillPayCounts()
        {
            return _billPayRepository.TotalAdminBillPayCount();
        }

        #endregion
    }
}
