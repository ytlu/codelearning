﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using NWBA.Domain.Entity;
using NWBA.Domain.Interface;
using NWBA.Logic.Interface.Service;

namespace NWBA.Logic.Service
{
    /// <summary>
    /// This class manage the paging detail which gets use by the website
    /// </summary>
    public class PagingService : BaseService, IPagingService
    {
        #region Private Members

        private int? _defaultPageSize;

        private int DefaultPageSize
        {
            get
            {
                if (!_defaultPageSize.HasValue)
                {
                    int defaultPageSize = 0;
                    if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["DefaultPageSize"]) &&
                        int.TryParse(ConfigurationManager.AppSettings["DefaultPageSize"], out defaultPageSize))
                    {
                        _defaultPageSize = (defaultPageSize <= 0 ? 4 : defaultPageSize);
                    }
                    else
                    {
                        // Fall back value
                        _defaultPageSize = 4;
                    }
                }

                return _defaultPageSize.Value;
            }
        }


        private int? _defaultPagingSize;
        private int DefaultPagingSize
        {
            get
            {
                if (!_defaultPagingSize.HasValue)
                {
                    int defaultPagingSize = 0;
                    if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["DefaultPagingSize"]) &&
                        int.TryParse(ConfigurationManager.AppSettings["DefaultPagingSize"], out defaultPagingSize))
                    {
                        _defaultPagingSize = (defaultPagingSize <= 0 ? 7 : defaultPagingSize);
                    }
                    else
                    {
                        // Fall back value
                        _defaultPagingSize = 7;
                    }
                }

                return _defaultPagingSize.Value;
            }
        }

        #endregion


        #region Constructor

        public PagingService()
            : base(typeof(PagingService))
        {
        }

        #endregion


        #region Public Methods

        /// <summary>
        /// This method returns the skip and take value for the Linq in order to doing paging
        /// </summary>
        /// <param name="selectedPage">Selected page</param>
        /// <returns>Record range to be use by the Linq</returns>
        public IRecordRangeModel GetRecordRange(int selectedPage)
        {
            var pageModel = new RecordRangeModel
            {
                RecordFrom = ((selectedPage - 1) * DefaultPageSize),
                NumberOfItem = DefaultPageSize
            };

            return pageModel;
        }

        /// <summary>
        /// Build the page model to be use by the paging control
        /// </summary>
        /// <param name="totalRecords">Total count of records</param>
        /// <param name="selectedPage">Selected page to be view by customer</param>
        /// <returns>Page model to be use by paging control</returns>
        public IPageModel BuildPageModel(int totalRecords, int selectedPage)
        {
            // Init page model with fixed configuration values
            var pageModel = new PageModel()
            {
                PageSize = DefaultPageSize,
                TotalRecords = totalRecords,
                SelectedPage = (selectedPage <= 0 ? 1 : selectedPage)
            };

            // Calculate the total page base on total records and default page size
            int totalPages = 0;
            if (totalRecords > 0 && DefaultPageSize > 0)
            {
                var remain = totalRecords % DefaultPageSize;
                totalPages = ((totalRecords - remain) / DefaultPageSize) + (remain > 0 ? 1 : 0);
            }

            pageModel.TotalPages = totalPages;

            pageModel.ShowPaging = (totalPages > 1);

            // Only need to build paging if there is more than one page.
            if (pageModel.ShowPaging)
            {
                // Setup viewing record range
                pageModel.RecordFrom = ((selectedPage - 1) * DefaultPageSize) + 1;
                pageModel.RecordTo = pageModel.RecordFrom + DefaultPageSize - 1;
                if (pageModel.RecordTo > totalRecords)
                    pageModel.RecordTo = totalRecords;

                var pagingRange = DefaultPagingSize / 2;

                // Setup paging range, i.e., << Prev 2, 3, 4, 5, 6 Next >>
                pageModel.StartPage = (selectedPage - pagingRange);
                pageModel.EndPage = (selectedPage + pagingRange);

                // Ensure last page does not go over the total page
                if (pageModel.EndPage > totalPages)
                {
                    pageModel.EndPage = totalPages;
                    pageModel.StartPage = pageModel.EndPage - DefaultPagingSize + 1;
                }

                // Make sure start page start from page is not less than 1
                if (pageModel.StartPage < 1)
                {
                    pageModel.StartPage = 1;
                }

                // Make sure the last page is the same size as total page or pagign size
                if (pageModel.StartPage == 1)
                {
                    if (pageModel.EndPage < DefaultPagingSize)
                    {
                        pageModel.EndPage = (DefaultPagingSize < totalPages ? DefaultPagingSize : totalPages);
                    }
                }

                // Setup previous, next
                pageModel.HasPrevious = (pageModel.StartPage > 1);
                pageModel.HasNext = (pageModel.EndPage < totalPages);
            }
            else
            {
                // If there is no page, set the record read detail
                // from 1 to the record end
                pageModel.RecordFrom = 1;
                pageModel.RecordTo = totalRecords;
            }

            return pageModel;
        }

        #endregion

    }
}
