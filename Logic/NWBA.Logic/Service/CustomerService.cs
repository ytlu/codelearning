﻿using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NWBA.Common;
using NWBA.Data;
using NWBA.Domain.Entity;
using NWBA.Domain.Interface;
using NWBA.Logic.Interface.Repository;
using NWBA.Logic.Interface.Service;
using NWBA.Logic.Repository;

namespace NWBA.Logic.Service
{
    /// <summary>
    /// This class manage the customer related operation
    /// </summary>
    public class CustomerService : BaseService, ICustomerService
    {
        #region Private Members

        private readonly ICustomerRepository _customerRepository;
        private readonly IAddressService _addressService;
        private readonly ILoginService _loginService;
        private readonly IBillPayService _billPayService;
        private readonly IPagingService _pagingService;

        #endregion


        #region Constructor

        public CustomerService(ICustomerRepository customerRepository, IAddressService addressService,
            ILoginService loginService, IBillPayService billPayService, IPagingService pagingService)
            : base(typeof(CustomerService))
        {
            _customerRepository = customerRepository;
            _addressService = addressService;
            _loginService = loginService;
            _billPayService = billPayService;
            _pagingService = pagingService;
        }

        #endregion


        #region Public Methods

        /// <summary>
        /// Get customer by customer ID
        /// </summary>
        /// <param name="customerID">Customer ID</param>
        /// <returns>Customer detail</returns>
        public ICustomer GetCustomerByID(int customerID)
        {
            return _customerRepository.GetCustomerByID(customerID);
        }

        /// <summary>
        /// Get customers list in the system
        /// </summary>
        /// <param name="selectedPage">Select page for the customer list</param>
        /// <returns>List of paged customers</returns>
        public ICustomerListModel GetCustomerList(int selectedPage)
        {
            var customerList = new CustomerListModel()
            {
                HasCustomer = false
            };

            var totalRecords = TotalCustomerCount();

            // Only try to retrive the customer records if total records for the customer is greater than 0
            if (totalRecords > 0)
            {
                customerList.HasCustomer = true;

                // Populate the paging model
                customerList.PageModel = _pagingService.BuildPageModel(totalRecords, selectedPage);

                // Get the records according to selected page
                var paging = _pagingService.GetRecordRange(selectedPage);

                // Retrieve the customer records with paging
                customerList.Customers =
                    _customerRepository.GetCustomers(paging.RecordFrom, paging.NumberOfItem);
            }

            return customerList;
        }

        /// <summary>
        /// Get customer by username
        /// </summary>
        /// <param name="userName">Customer login name</param>
        /// <returns>Customer detail</returns>
        public ICustomer GetCustomerByUserName(string userName)
        {
            return _customerRepository.GetCustomerByUserName(userName);
        }

        /// <summary>
        /// Get customer ID by user name, this is to reduce data transfer
        /// </summary>
        /// <param name="userName"></param>
        /// <returns>Customer ID</returns>
        public int GetCustomerIDByUserName(string userName)
        {
            return _customerRepository.GetCustomerIDByUserName(userName);
        }

        /// <summary>
        /// Get address of the customer
        /// </summary>
        /// <param name="customerID">Customer ID</param>
        /// <returns>Address detail of the customer</returns>
        public IAddress GetAddressByCustomerID(int customerID)
        {
            return _addressService.GetAddressByCustomerID(customerID);
        }

        /// <summary>
        /// Get customer account number of customer
        /// </summary>
        /// <param name="customerID">Customer ID</param>
        /// <returns>List of customer account number</returns>
        public IList<int> GetCustomerAccountNumberList(int customerID)
        {
            return _customerRepository.GetCustomerAccountList(customerID);
        }

        /// <summary>
        /// Save customer detail to the database
        /// </summary>
        /// <param name="customer">Customer details</param>
        /// <returns>Customer ID which gets updated</returns>
        public int SaveCustomer(ICustomer customer)
        {
            return _customerRepository.SaveCustomer(customer);
        }

        /// <summary>
        /// Update the password of the customer
        /// </summary>
        /// <param name="customerID">Customer ID</param>
        /// <param name="userName">Customer username</param>
        /// <param name="currentPassword">Current password</param>
        /// <param name="newPassword">New password</param>
        public void UpdatePassword(int customerID, string userName, string currentPassword, string newPassword)
        {
            // We need to verify if the current customer ID and the username is belong to the same account
            // before making change to the customer password
            _customerRepository.VerifyLoginByCustomerIDandUserName(customerID, userName);

            _loginService.UpdatePassword(userName, currentPassword, newPassword);

        }

        /// <summary>
        /// Get bill pay of customer
        /// </summary>
        /// <param name="customerID">Customer ID</param>
        /// <returns>List of customer bill pays</returns>
        public IBillPayRecord GetBillPays(int customerID, int selectedPage)
        {
            return _billPayService.GetBillPays(customerID, selectedPage);
        }

        #endregion


        #region Helper Methods

        /// <summary>
        /// Get total count of customer in the system
        /// </summary>
        /// <returns>Total count of the customers</returns>
        private int TotalCustomerCount()
        {
            return _customerRepository.TotalCustomerCount();
        }

        #endregion
    }
}
