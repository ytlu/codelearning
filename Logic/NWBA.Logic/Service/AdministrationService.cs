﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NWBA.Domain.Interface;
using NWBA.Logic.Interface.Service;
using NWBA.Logic.Repository;

namespace NWBA.Logic.Service
{
    /// <summary>
    /// This class manage administration operation
    /// </summary>
    public class AdministrationService : BaseService, IAdministrationService
    {
        #region Private Members

        private readonly ICustomerService _customerService;
        private readonly IBillPayService _billPayService;

        #endregion


        #region Constructor

        public AdministrationService(ICustomerService customerService, IBillPayService billPayService)
            : base(typeof(AdministrationService))
        {
            _customerService = customerService;
            _billPayService = billPayService;
        }

        #endregion


        #region Public Methods

        /// <summary>
        /// Get customer list
        /// </summary>
        /// <param name="selectedPage">Selected page for customer</param>
        /// <returns>List of customers for selected page</returns>
        public ICustomerListModel GetCustomerList(int selectedPage)
        {
            return _customerService.GetCustomerList(selectedPage);
        }

        /// <summary>
        /// Get customer bill pay list
        /// </summary>
        /// <param name="selectedPage">Selected page for customer bill pay</param>
        /// <returns>List of customer bill pay for selected page</returns>
        public IAdminSchedulePaymentModel GetAdminBillPays(int selectedPage)
        {
            return _billPayService.GetAdminBillPays(selectedPage);
        }

        /// <summary>
        /// Enable the bill pay for the payment scheduler
        /// </summary>
        /// <param name="billPayID">Bill pay ID</param>
        public void EnableBillPay(int billPayID)
        {
            GetLogger.Debug("Calling::EnableBillPay");
            GetLogger.DebugFormat("Enable account: {0}", billPayID);
            _billPayService.EnableBillPay(billPayID);
        }

        /// <summary>
        /// Pause the bill pay for the payment scheduler
        /// </summary>
        /// <param name="billPayID">Bill pay ID</param>
        public void PauseBillPay(int billPayID)
        {
            GetLogger.Debug("Calling::PauseBillPay");
            GetLogger.DebugFormat("Enable account: {0}", billPayID);
            _billPayService.PauseBillPay(billPayID);
        }

        #endregion
    }
}