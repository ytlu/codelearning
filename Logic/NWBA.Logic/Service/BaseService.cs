﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace NWBA.Logic.Service
{
    public abstract class BaseService
    {
        #region Private Members

        private ILog _log;

        #endregion


        #region Protected Members

        protected ILog GetLogger { get { return _log; } }

        #endregion


        #region Constructor

        protected BaseService(Type concreteType)
        {
            _log = LogManager.GetLogger(concreteType);
        }

        #endregion
    }
}
