﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NWBA.Domain.Interface;
using NWBA.Logic.Interface.Repository;
using NWBA.Logic.Interface.Service;

namespace NWBA.Logic.Service
{
    /// <summary>
    /// This class manage address related operation
    /// </summary>
    public class AddressService : BaseService, IAddressService
    {
        #region Private Members

        private readonly IAddressRepository _addressRepository;

        #endregion


        #region Constructor

        public AddressService(IAddressRepository addressRepository)
            : base(typeof(AddressService))
        {
            _addressRepository = addressRepository;
        }

        #endregion


        #region Public Methods

        /// <summary>
        /// Get customer address
        /// </summary>
        /// <param name="customerID">Customer ID</param>
        /// <returns>Address which belong to the customer</returns>
        public IAddress GetAddressByCustomerID(int customerID)
        {
            return _addressRepository.GetAddressByCustomerID(customerID);
        }

        /// <summary>
        /// Save the address into database and return the address ID
        /// </summary>
        /// <param name="address">Address detail to be saved to database</param>
        /// <returns>Address ID been saved</returns>
        public int SaveAddress(IAddress address)
        {
            return _addressRepository.SaveAddress(address);
        }

        #endregion
    }
}
