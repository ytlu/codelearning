﻿using System.Collections.Generic;
using NWBA.Domain.Interface;
using NWBA.Logic.Interface.Repository;
using NWBA.Logic.Interface.Service;

namespace NWBA.Logic.Service
{
    /// <summary>
    /// This class manage payee operation
    /// </summary>
    public class PayeeService : BaseService, IPayeeService
    {
        #region Private Members

        private readonly IPayeeRepository _payeeRepository;

        #endregion


        #region Constructor

        public PayeeService(IPayeeRepository payeeRepository)
            : base(typeof(PayeeService))
        {
            _payeeRepository = payeeRepository;
        }

        #endregion


        #region Public Methods

        /// <summary>
        /// Get shared payee list
        /// </summary>
        /// <returns>List of shared payee</returns>
        public IList<IPayee> GetPayess()
        {
            return _payeeRepository.GetPayeeList();
        }

        #endregion
    }
}
