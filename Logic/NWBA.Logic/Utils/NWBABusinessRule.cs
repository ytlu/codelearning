﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NWBA.Logic.Utils
{
    /// <summary>
    /// This class contains internal static method for NWBA business rule
    /// </summary>
    internal class NWBABusinessRule
    {

        #region Account Rule

        /// <summary>
        /// This class contains the rules which applied to account
        /// </summary>
        internal class AccountRule
        {
            #region Internal Statis Methods

            /// <summary>
            /// This method returns the minimum balance allowed for an account type
            /// </summary>
            /// <param name="accountType">Account type</param>
            /// <returns>Minimum balance allowed for the account type</returns>
            internal static decimal MinimumBalanceAllowed(string accountType)
            {
                return (accountType == "S" ? 0 : 200);
            }

            /// <summary>
            /// Return the max free transaction allowed per customer
            /// </summary>
            internal static int MaxFreeTransactionAllowed
            {
                get { return NWBAConfig.MAX_FREE_TRANSACTION_ALLOWED; }
            }

            #endregion
        }

        #endregion


        #region Transaction Rule

        /// <summary>
        /// This class contains the rules which applied to transaction
        /// </summary>
        internal class TransactionRule
        {
            #region Internal Static Methods

            /// <summary>
            /// Check if the transaction is credit type (i.e., deposit)
            /// </summary>
            /// <param name="transactionType">Transaction type</param>
            /// <returns>True if the transaction is credit</returns>
            internal static bool IsCreditTransaction(string transactionType)
            {
                return (transactionType == "D");
            }

            /// <summary>
            /// Get third charge description
            /// </summary>
            /// <param name="transactionType">Transaction type</param>
            /// <returns>Readable comment for the third charge to add to transaction</returns>
            internal static string GetThirdChargeDescription(string transactionType)
            {
                var surchargeDesc = string.Empty;
                switch (transactionType)
                {
                    case "W":
                        surchargeDesc = "Service charge - ATM transaction";
                        break;
                    case "B":
                        surchargeDesc = "Service charge - Internet BPAY";
                        break;
                    case "T":
                        surchargeDesc = "Service charge - Funds Transfer";
                        break;

                }
                return surchargeDesc;
            }

            /// <summary>
            /// Get the third charge amount for the type of transaction
            /// </summary>
            /// <param name="transactionType">Transaction type to be executed</param>
            /// <returns>Any third charge applied to the transaction type</returns>
            internal static decimal GetSurChargeAmount(string transactionType)
            {
                decimal surchargeAmount = 0;
                switch (transactionType)
                {
                    case "W":
                        surchargeAmount = 0.2M;
                        break;
                    case "B":
                        surchargeAmount = 0.3M;
                        break;
                    case "T":
                        surchargeAmount = 0.2M;
                        break;

                }
                return surchargeAmount;
            }

            #endregion
        }

        #endregion

    }
}
