﻿using System.Net;
using AutoMapper;
using NWBA.Domain.Entity;
using NWBA.Domain.Interface;

namespace NWBA.Logic.Utils
{
    /// <summary>
    /// This class handles the object mapping between database model against Domain entities model.
    /// This class is design as singleton to ensure the maaping only executed once
    /// </summary>
    public sealed class DataTransformer
    {
        #region Private Static Object

        private static DataTransformer _instance;
        private static object _dtLocker = new object();

        #endregion


        #region Private Members

        private bool _hasInitMapping = false;

        #endregion


        #region Constructor

        private DataTransformer() { }

        #endregion


        #region Public Static Methods

        public static DataTransformer Instance
        {
            get
            {
                if (_instance == null)
                {
                    // The lock is to prevent creation of the same instance object in multithread application
                    lock (_dtLocker)
                    {
                        _instance = new DataTransformer();
                    }
                }

                return _instance;
            }
        }

        #endregion


        #region Public Methods

        /// <summary>
        /// This method ensure the mapping been called only once
        /// </summary>
        public void EnsureMapping()
        {
            if (!_hasInitMapping)
            {
                // Mark off mapping been called
                _hasInitMapping = true;

                // Now setup the mapping, for one time only
                SetupDomainMapping();
            }
        }

        #endregion


        #region Helper Methods

        /// <summary>
        /// This method will setup the object mapping
        /// </summary>
        private void SetupDomainMapping()
        {
            Mapper.CreateMap<Data.Login, Login>();

            Mapper.CreateMap<Data.Customer, Customer>();
            Mapper.CreateMap<ICustomer, Data.Customer>();

            Mapper.CreateMap<Data.Address, Address>();
            Mapper.CreateMap<IAddress, Data.Address>();

            Mapper.CreateMap<Data.Account, Account>()
                .ForMember(
                    acc => acc.AccountTypeDisplay,
                    acc => acc.MapFrom(data => (data.AccountType == "C" ? "Credits" : "Saving")));


            Mapper.CreateMap<IAccount, Data.Account>();

            Mapper.CreateMap<Data.Transaction, Transaction>()
                .ForMember(
                    tranx => tranx.TransactionTypeDisplay,
                    tranx => tranx.MapFrom(data => MapTransactionTypeToDesc(data.TransactionType))
                );

            Mapper.CreateMap<ITransaction, Data.Transaction>();

            Mapper.CreateMap<IBillPay, Data.BillPay>();
            Mapper.CreateMap<Data.BillPay, BillPay>();

            Mapper.CreateMap<Data.Payee, Payee>();

            Mapper.CreateMap<Data.spGetBillPayList_Result, CustomerPayBill>()
                .ForMember(
                    bill => bill.PeriodDisplay,
                    bill => bill.MapFrom(data => MapPeriodToDesc(data.Period))
                );

            Mapper.CreateMap<Data.spGetAdminBillPayList_Result, AdminBillPayRecord>()
                .ForMember(
                    bill => bill.PeriodDisplay,
                    bill => bill.MapFrom(data => MapPeriodToDesc(data.Period))
                )
                .ForMember(
                    bill => bill.AllowPause,
                    bill => bill.MapFrom(data => data.Status == 1)
                );
        }

        /// <summary>
        /// This method returns readable transaction type to the customer
        /// </summary>
        /// <param name="transactionType">Transaction type</param>
        /// <returns>Readable transaction type description</returns>
        internal static string MapTransactionTypeToDesc(string transactionType)
        {
            var transactionTypeDesc = string.Empty;

            switch (transactionType)
            {
                case "D":
                    transactionTypeDesc = "Deposit";
                    break;
                case "W":
                    transactionTypeDesc = "Withdrawal";
                    break;
                case "T":
                    transactionTypeDesc = "Transfer";
                    break;
                case "S":
                    transactionTypeDesc = "Service Charge";
                    break;
                case "B":
                    transactionTypeDesc = "BillPay";
                    break;
            }

            return transactionTypeDesc;
        }

        /// <summary>
        /// This method returns readable period type to the customer
        /// </summary>
        /// <param name="periodType">Period type</param>
        /// <returns>Readable period type description</returns>
        internal static string MapPeriodToDesc(string periodType)
        {
            var transactionTypeDesc = string.Empty;

            switch (periodType)
            {
                case "O":
                    transactionTypeDesc = "One time";
                    break;
                case "M":
                    transactionTypeDesc = "Monthly";
                    break;
                case "Q":
                    transactionTypeDesc = "Quarterly";
                    break;
                case "S":
                    transactionTypeDesc = "Service Charge";
                    break;
                case "Y":
                    transactionTypeDesc = "Yearly";
                    break;
            }

            return transactionTypeDesc;
        }
        #endregion
    }
}
