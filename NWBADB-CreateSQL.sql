USE [master]
GO
/****** Object:  Database [NWBADB]    Script Date: 10/08/2014 5:19:49 PM ******/
CREATE DATABASE [NWBADB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'NWBADB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\NWBADB.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'NWBADB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\NWBADB_log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [NWBADB] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [NWBADB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [NWBADB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [NWBADB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [NWBADB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [NWBADB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [NWBADB] SET ARITHABORT OFF 
GO
ALTER DATABASE [NWBADB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [NWBADB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [NWBADB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [NWBADB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [NWBADB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [NWBADB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [NWBADB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [NWBADB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [NWBADB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [NWBADB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [NWBADB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [NWBADB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [NWBADB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [NWBADB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [NWBADB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [NWBADB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [NWBADB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [NWBADB] SET RECOVERY FULL 
GO
ALTER DATABASE [NWBADB] SET  MULTI_USER 
GO
ALTER DATABASE [NWBADB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [NWBADB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [NWBADB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [NWBADB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [NWBADB] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'NWBADB', N'ON'
GO
USE [NWBADB]
GO
/****** Object:  User [nwba]    Script Date: 10/08/2014 5:19:49 PM ******/
CREATE LOGIN nwba WITH PASSWORD = 'nwba123';
GO
CREATE USER nwba FOR LOGIN nwba WITH DEFAULT_SCHEMA = [dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [nwba]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 10/08/2014 5:19:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[AccountNumber] [int] IDENTITY(1,1) NOT NULL,
	[AccountType] [nvarchar](1) NOT NULL,
	[CustomerID] [int] NOT NULL,
	[Balance] [money] NOT NULL,
	[ModifyDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED 
(
	[AccountNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Address]    Script Date: 10/08/2014 5:19:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Address](
	[AddressID] [int] IDENTITY(1,1) NOT NULL,
	[Street] [nvarchar](50) NOT NULL,
	[City] [nvarchar](40) NOT NULL,
	[State] [nvarchar](10) NOT NULL,
	[PostCode] [smallint] NOT NULL,
	[Phone] [nvarchar](15) NOT NULL,
 CONSTRAINT [PK_Address] PRIMARY KEY CLUSTERED 
(
	[AddressID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BillPay]    Script Date: 10/08/2014 5:19:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BillPay](
	[BillPayID] [int] IDENTITY(1,1) NOT NULL,
	[AccountNumber] [int] NOT NULL,
	[PayeeID] [int] NOT NULL,
	[Amount] [money] NOT NULL,
	[ScheduleDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[Period] [nvarchar](1) NOT NULL,
	[ModifyDate] [datetime] NOT NULL,
 CONSTRAINT [PK_BillPay] PRIMARY KEY CLUSTERED 
(
	[BillPayID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Customer]    Script Date: 10/08/2014 5:19:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer](
	[CustomerID] [int] IDENTITY(1,1) NOT NULL,
	[LoginID] [int] NOT NULL,
	[CustomerName] [nvarchar](50) NOT NULL,
	[TFN] [nvarchar](11) NULL,
	[AddressID] [int] NOT NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Login]    Script Date: 10/08/2014 5:19:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Login](
	[LoginID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [nvarchar](50) NOT NULL,
	[Password] [varchar](255) NOT NULL,
	[IsAdmin] [bit] NOT NULL,
	[ModifyDate] [datetime] NOT NULL CONSTRAINT [DF_Login_ModifyDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_Login] PRIMARY KEY CLUSTERED 
(
	[LoginID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Payee]    Script Date: 10/08/2014 5:19:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Payee](
	[PayeeID] [int] IDENTITY(1,1) NOT NULL,
	[PayeeName] [nvarchar](50) NOT NULL,
	[AddressID] [int] NOT NULL,
 CONSTRAINT [PK_Payee] PRIMARY KEY CLUSTERED 
(
	[PayeeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Transaction]    Script Date: 10/08/2014 5:19:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Transaction](
	[TransactionID] [int] IDENTITY(1,1) NOT NULL,
	[TransactionType] [nvarchar](1) NOT NULL,
	[AccountNumber] [int] NOT NULL,
	[DestAccount] [int] NULL,
	[Amount] [money] NOT NULL,
	[Comment] [nvarchar](255) NULL,
	[ModifyDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Transaction] PRIMARY KEY CLUSTERED 
(
	[TransactionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_Customer] FOREIGN KEY([CustomerID])
REFERENCES [dbo].[Customer] ([CustomerID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_Customer]
GO
ALTER TABLE [dbo].[BillPay]  WITH CHECK ADD  CONSTRAINT [FK_BillPay_Account] FOREIGN KEY([AccountNumber])
REFERENCES [dbo].[Account] ([AccountNumber])
GO
ALTER TABLE [dbo].[BillPay] CHECK CONSTRAINT [FK_BillPay_Account]
GO
ALTER TABLE [dbo].[BillPay]  WITH CHECK ADD  CONSTRAINT [FK_BillPay_Payee] FOREIGN KEY([PayeeID])
REFERENCES [dbo].[Payee] ([PayeeID])
GO
ALTER TABLE [dbo].[BillPay] CHECK CONSTRAINT [FK_BillPay_Payee]
GO
ALTER TABLE [dbo].[Customer]  WITH CHECK ADD  CONSTRAINT [FK_Customer_Address] FOREIGN KEY([AddressID])
REFERENCES [dbo].[Address] ([AddressID])
GO
ALTER TABLE [dbo].[Customer] CHECK CONSTRAINT [FK_Customer_Address]
GO
ALTER TABLE [dbo].[Customer]  WITH CHECK ADD  CONSTRAINT [FK_Customer_Login] FOREIGN KEY([LoginID])
REFERENCES [dbo].[Login] ([LoginID])
GO
ALTER TABLE [dbo].[Customer] CHECK CONSTRAINT [FK_Customer_Login]
GO
ALTER TABLE [dbo].[Payee]  WITH CHECK ADD  CONSTRAINT [FK_Payee_Address] FOREIGN KEY([AddressID])
REFERENCES [dbo].[Address] ([AddressID])
GO
ALTER TABLE [dbo].[Payee] CHECK CONSTRAINT [FK_Payee_Address]
GO
ALTER TABLE [dbo].[Transaction]  WITH CHECK ADD  CONSTRAINT [FK_Transaction_Account] FOREIGN KEY([AccountNumber])
REFERENCES [dbo].[Account] ([AccountNumber])
GO
ALTER TABLE [dbo].[Transaction] CHECK CONSTRAINT [FK_Transaction_Account]
GO
ALTER TABLE [dbo].[Transaction]  WITH CHECK ADD  CONSTRAINT [FK_Transaction_Account_Dest] FOREIGN KEY([DestAccount])
REFERENCES [dbo].[Account] ([AccountNumber])
GO
ALTER TABLE [dbo].[Transaction] CHECK CONSTRAINT [FK_Transaction_Account_Dest]
GO
/****** Object:  StoredProcedure [dbo].[spGetChargableTransaction]    Script Date: 10/08/2014 5:19:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetChargableTransaction]
	@AccountNumber INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @chargableTransaction INT

	SELECT @chargableTransaction = Count(TransactionID) 
	FROM   [Transaction] 
	WHERE  AccountNumber = @AccountNumber
		   AND ( TransactionType IN ( 'W', 'B' ) 
				  OR ( TransactionType = 'T' AND Amount < 0 ) ) 

	SELECT ISNULL(@chargableTransaction, 0) AS [ChargableTransaction]

END

GO

/****** Object:  StoredProcedure [dbo].[spGetBillPayList]    Script Date: 23/08/2014 5:54:49 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetBillPayList]
@CustomerID INT,
@SkipRow INT,
@TakeRow INT
AS
BEGIN
	SET NOCOUNT ON;

	WITH Paged AS (
		SELECT bp.BillPayID, 
			   bp.AccountNumber, 
			   acc.Balance, 
			   pp.PayeeName, 
			   bp.Period,
			   bp.Amount, 
			   bp.ScheduleDate,
			   ROW_NUMBER() OVER (ORDER  BY ScheduleDate) AS RowNum
		FROM   BillPay bp 
			   INNER JOIN Account acc 
					   ON bp.AccountNumber = acc.AccountNumber 
			   INNER JOIN Payee pp 
					   ON bp.PayeeID = pp.PayeeID
		WHERE  acc.CustomerID = @CustomerID AND bp.Status = 1)

		SELECT TOP (@TakeRow) BillPayID, 
			   AccountNumber, 
			   Balance, 
			   PayeeName, 
			   Period,
			   Amount, 
			   ScheduleDate
		FROM   Paged
		WHERE  RowNum > @SkipRow
		ORDER  BY ScheduleDate
END

GO

/****** Object:  StoredProcedure [dbo].[spGetAdminBillPayList]    Script Date: 24/08/2014 11:00:49 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetAdminBillPayList]
@SkipRow INT,
@TakeRow INT
AS
BEGIN
	SET NOCOUNT ON;

	WITH Paged AS (
		SELECT cc.CustomerID,
			   cc.CustomerName, 
			   bp.AccountNumber, 
			   acc.Balance, 
			   bp.BillPayID, 
			   pp.PayeeName, 
			   bp.Period,
			   bp.Amount, 
			   bp.ScheduleDate,
			   bp.[Status],
			   ROW_NUMBER() OVER (ORDER BY cc.CustomerID, bp.ScheduleDate) AS RowNum
		FROM   Customer cc
			   INNER JOIN Account acc 
					   ON acc.CustomerID = cc.CustomerID
			   INNER JOIN BillPay bp 
					   ON bp.AccountNumber = acc.AccountNumber 
			   INNER JOIN Payee pp 
					   ON bp.PayeeID = pp.PayeeID
		WHERE  bp.Status IN (1, 3))

		SELECT TOP (@TakeRow) 
			   CustomerID, 
			   CustomerName, 
			   AccountNumber, 
			   Balance, 
			   BillPayID, 
			   PayeeName, 
			   Period,
			   Amount, 
			   ScheduleDate,
			   [Status]
		FROM   Paged
		WHERE  RowNum > @SkipRow
		ORDER  BY CustomerID, ScheduleDate
END

GO

USE [master]
GO
ALTER DATABASE [NWBADB] SET  READ_WRITE 
GO

USE [NWBADB]
GO

/**CREATE Address**/
INSERT INTO [dbo].[Address] ([Street],[City],[State],[PostCode],[Phone]) VALUES ('1 Street St','Eagle Vale','NSW',2558,'987 6510');
INSERT INTO [dbo].[Address] ([Street],[City],[State],[PostCode],[Phone]) VALUES ('2 Street St','Eagleton','NSW',2324,'987 6511');
INSERT INTO [dbo].[Address] ([Street],[City],[State],[PostCode],[Phone]) VALUES ('3 Street St','Earlwood','NSW',2206,'987 6512');
INSERT INTO [dbo].[Address] ([Street],[City],[State],[PostCode],[Phone]) VALUES ('4 Street St','East Albury','NSW',2640,'987 6513');
INSERT INTO [dbo].[Address] ([Street],[City],[State],[PostCode],[Phone]) VALUES ('13 Street St','Macclesfield','VIC',3782,'987 6522');
INSERT INTO [dbo].[Address] ([Street],[City],[State],[PostCode],[Phone]) VALUES ('14 Street St','Macedon','VIC',3440,'987 6523');
INSERT INTO [dbo].[Address] ([Street],[City],[State],[PostCode],[Phone]) VALUES ('16 Street St','Macleod','VIC',3085,'987 6525');
INSERT INTO [dbo].[Address] ([Street],[City],[State],[PostCode],[Phone]) VALUES ('25 Street St','Jamboree Heights','QLD',4074,'987 6534');
INSERT INTO [dbo].[Address] ([Street],[City],[State],[PostCode],[Phone]) VALUES ('26 Street St','James Cook University','QLD',4811,'987 6535');
INSERT INTO [dbo].[Address] ([Street],[City],[State],[PostCode],[Phone]) VALUES ('27 Street St','Jandowae','QLD',4410,'987 6536');
GO

/**Create LOGIN**/
INSERT INTO [dbo].[Login] ([UserID],[Password],[IsAdmin],[ModifyDate]) VALUES('user1','CDOM0B9Q29oIqyUlE52flzQ0R3U=',0,GETDATE())
INSERT INTO [dbo].[Login] ([UserID],[Password],[IsAdmin],[ModifyDate]) VALUES('user2','CDOM0B9Q29oIqyUlE52flzQ0R3U=',0,GETDATE())
INSERT INTO [dbo].[Login] ([UserID],[Password],[IsAdmin],[ModifyDate]) VALUES('user3','CDOM0B9Q29oIqyUlE52flzQ0R3U=',0,GETDATE())
INSERT INTO [dbo].[Login] ([UserID],[Password],[IsAdmin],[ModifyDate]) VALUES('user4','CDOM0B9Q29oIqyUlE52flzQ0R3U=',0,GETDATE())
INSERT INTO [dbo].[Login] ([UserID],[Password],[IsAdmin],[ModifyDate]) VALUES('user5','CDOM0B9Q29oIqyUlE52flzQ0R3U=',0,GETDATE())
INSERT INTO [dbo].[Login] ([UserID],[Password],[IsAdmin],[ModifyDate]) VALUES('user6','CDOM0B9Q29oIqyUlE52flzQ0R3U=',0,GETDATE())
INSERT INTO [dbo].[Login] ([UserID],[Password],[IsAdmin],[ModifyDate]) VALUES('user7','CDOM0B9Q29oIqyUlE52flzQ0R3U=',0,GETDATE())
INSERT INTO [dbo].[Login] ([UserID],[Password],[IsAdmin],[ModifyDate]) VALUES('user8','CDOM0B9Q29oIqyUlE52flzQ0R3U=',0,GETDATE())
INSERT INTO [dbo].[Login] ([UserID],[Password],[IsAdmin],[ModifyDate]) VALUES('user9','CDOM0B9Q29oIqyUlE52flzQ0R3U=',0,GETDATE())
INSERT INTO [dbo].[Login] ([UserID],[Password],[IsAdmin],[ModifyDate]) VALUES('user10','CDOM0B9Q29oIqyUlE52flzQ0R3U=',0,GETDATE())

INSERT INTO [dbo].[Login] ([UserID],[Password],[IsAdmin],[ModifyDate]) VALUES('admin','CDOM0B9Q29oIqyUlE52flzQ0R3U=',1,GETDATE())
GO

/**Create Customer**/
INSERT INTO [dbo].[Customer] ([LoginID],[CustomerName],[TFN],[AddressID]) VALUES (1,'Customer No1','153578657',1)
INSERT INTO [dbo].[Customer] ([LoginID],[CustomerName],[TFN],[AddressID]) VALUES (2,'Customer No2','153578658',2)
INSERT INTO [dbo].[Customer] ([LoginID],[CustomerName],[TFN],[AddressID]) VALUES (3,'Customer No3','153578659',3)
INSERT INTO [dbo].[Customer] ([LoginID],[CustomerName],[TFN],[AddressID]) VALUES (4,'Customer No4','153578660',4)
INSERT INTO [dbo].[Customer] ([LoginID],[CustomerName],[TFN],[AddressID]) VALUES (5,'Customer No5','153578661',5)
INSERT INTO [dbo].[Customer] ([LoginID],[CustomerName],[TFN],[AddressID]) VALUES (6,'Customer No6','153578662',6)
INSERT INTO [dbo].[Customer] ([LoginID],[CustomerName],[TFN],[AddressID]) VALUES (7,'Customer No7','153578663',7)
INSERT INTO [dbo].[Customer] ([LoginID],[CustomerName],[TFN],[AddressID]) VALUES (8,'Customer No8','153578664',8)
INSERT INTO [dbo].[Customer] ([LoginID],[CustomerName],[TFN],[AddressID]) VALUES (9,'Customer No9','153578665',9)
INSERT INTO [dbo].[Customer] ([LoginID],[CustomerName],[TFN],[AddressID]) VALUES (10,'Customer No10','153578666',10)
GO


/**Create Account**/
SET DATEFORMAT YMD
INSERT INTO [dbo].[Account] ([AccountType],[CustomerID],[Balance],[ModifyDate]) VALUES ('S',1,1500,'2014/08/01')
INSERT INTO [dbo].[Account] ([AccountType],[CustomerID],[Balance],[ModifyDate]) VALUES ('C',1,2000,'2014/08/01')
INSERT INTO [dbo].[Account] ([AccountType],[CustomerID],[Balance],[ModifyDate]) VALUES ('S',1,2500,'2014/08/01')
INSERT INTO [dbo].[Account] ([AccountType],[CustomerID],[Balance],[ModifyDate]) VALUES ('S',2,3000,'2014/08/01')
INSERT INTO [dbo].[Account] ([AccountType],[CustomerID],[Balance],[ModifyDate]) VALUES ('C',2,3500,'2014/08/01')
INSERT INTO [dbo].[Account] ([AccountType],[CustomerID],[Balance],[ModifyDate]) VALUES ('S',2,4000,'2014/08/01')
INSERT INTO [dbo].[Account] ([AccountType],[CustomerID],[Balance],[ModifyDate]) VALUES ('S',3,4500,'2014/08/01')
INSERT INTO [dbo].[Account] ([AccountType],[CustomerID],[Balance],[ModifyDate]) VALUES ('C',3,5000,'2014/08/01')
INSERT INTO [dbo].[Account] ([AccountType],[CustomerID],[Balance],[ModifyDate]) VALUES ('S',3,5500,'2014/08/01')
INSERT INTO [dbo].[Account] ([AccountType],[CustomerID],[Balance],[ModifyDate]) VALUES ('S',4,6000,'2014/08/01')
INSERT INTO [dbo].[Account] ([AccountType],[CustomerID],[Balance],[ModifyDate]) VALUES ('C',4,6500,'2014/08/01')
INSERT INTO [dbo].[Account] ([AccountType],[CustomerID],[Balance],[ModifyDate]) VALUES ('S',4,7000,'2014/08/01')
INSERT INTO [dbo].[Account] ([AccountType],[CustomerID],[Balance],[ModifyDate]) VALUES ('S',5,7500,'2014/08/01')
INSERT INTO [dbo].[Account] ([AccountType],[CustomerID],[Balance],[ModifyDate]) VALUES ('C',5,8000,'2014/08/01')
INSERT INTO [dbo].[Account] ([AccountType],[CustomerID],[Balance],[ModifyDate]) VALUES ('S',5,8500,'2014/08/01')
INSERT INTO [dbo].[Account] ([AccountType],[CustomerID],[Balance],[ModifyDate]) VALUES ('S',6,1500,'2014/08/01')
INSERT INTO [dbo].[Account] ([AccountType],[CustomerID],[Balance],[ModifyDate]) VALUES ('C',6,2000,'2014/08/01')
INSERT INTO [dbo].[Account] ([AccountType],[CustomerID],[Balance],[ModifyDate]) VALUES ('S',6,2500,'2014/08/01')
INSERT INTO [dbo].[Account] ([AccountType],[CustomerID],[Balance],[ModifyDate]) VALUES ('S',7,3000,'2014/08/01')
INSERT INTO [dbo].[Account] ([AccountType],[CustomerID],[Balance],[ModifyDate]) VALUES ('C',7,3500,'2014/08/01')
INSERT INTO [dbo].[Account] ([AccountType],[CustomerID],[Balance],[ModifyDate]) VALUES ('S',7,4000,'2014/08/01')
INSERT INTO [dbo].[Account] ([AccountType],[CustomerID],[Balance],[ModifyDate]) VALUES ('S',8,4500,'2014/08/01')
INSERT INTO [dbo].[Account] ([AccountType],[CustomerID],[Balance],[ModifyDate]) VALUES ('C',8,5000,'2014/08/01')
INSERT INTO [dbo].[Account] ([AccountType],[CustomerID],[Balance],[ModifyDate]) VALUES ('S',8,5500,'2014/08/01')
INSERT INTO [dbo].[Account] ([AccountType],[CustomerID],[Balance],[ModifyDate]) VALUES ('S',9,6000,'2014/08/01')
INSERT INTO [dbo].[Account] ([AccountType],[CustomerID],[Balance],[ModifyDate]) VALUES ('C',9,6500,'2014/08/01')
INSERT INTO [dbo].[Account] ([AccountType],[CustomerID],[Balance],[ModifyDate]) VALUES ('S',9,7000,'2014/08/01')
INSERT INTO [dbo].[Account] ([AccountType],[CustomerID],[Balance],[ModifyDate]) VALUES ('S',10,7500,'2014/08/01')
INSERT INTO [dbo].[Account] ([AccountType],[CustomerID],[Balance],[ModifyDate]) VALUES ('C',10,8000,'2014/08/01')
INSERT INTO [dbo].[Account] ([AccountType],[CustomerID],[Balance],[ModifyDate]) VALUES ('S',10,8500,'2014/08/01')


GO

/**Create Transaction**/
SET DATEFORMAT YMD
INSERT INTO [dbo].[Transaction] ([TransactionType],[AccountNumber],[DestAccount],[Amount],[Comment],[ModifyDate]) VALUES ('D',1,NULL,1500,'Open Balance','2014/08/01')
INSERT INTO [dbo].[Transaction] ([TransactionType],[AccountNumber],[DestAccount],[Amount],[Comment],[ModifyDate]) VALUES ('D',2,NULL,2000,'Open Balance','2014/08/01')
INSERT INTO [dbo].[Transaction] ([TransactionType],[AccountNumber],[DestAccount],[Amount],[Comment],[ModifyDate]) VALUES ('D',3,NULL,2500,'Open Balance','2014/08/01')
INSERT INTO [dbo].[Transaction] ([TransactionType],[AccountNumber],[DestAccount],[Amount],[Comment],[ModifyDate]) VALUES ('D',4,NULL,3000,'Open Balance','2014/08/01')
INSERT INTO [dbo].[Transaction] ([TransactionType],[AccountNumber],[DestAccount],[Amount],[Comment],[ModifyDate]) VALUES ('D',5,NULL,3500,'Open Balance','2014/08/01')
INSERT INTO [dbo].[Transaction] ([TransactionType],[AccountNumber],[DestAccount],[Amount],[Comment],[ModifyDate]) VALUES ('D',6,NULL,4000,'Open Balance','2014/08/01')
INSERT INTO [dbo].[Transaction] ([TransactionType],[AccountNumber],[DestAccount],[Amount],[Comment],[ModifyDate]) VALUES ('D',7,NULL,4500,'Open Balance','2014/08/01')
INSERT INTO [dbo].[Transaction] ([TransactionType],[AccountNumber],[DestAccount],[Amount],[Comment],[ModifyDate]) VALUES ('D',8,NULL,5000,'Open Balance','2014/08/01')
INSERT INTO [dbo].[Transaction] ([TransactionType],[AccountNumber],[DestAccount],[Amount],[Comment],[ModifyDate]) VALUES ('D',9,NULL,5500,'Open Balance','2014/08/01')
INSERT INTO [dbo].[Transaction] ([TransactionType],[AccountNumber],[DestAccount],[Amount],[Comment],[ModifyDate]) VALUES ('D',10,NULL,6000,'Open Balance','2014/08/01')
INSERT INTO [dbo].[Transaction] ([TransactionType],[AccountNumber],[DestAccount],[Amount],[Comment],[ModifyDate]) VALUES ('D',11,NULL,6500,'Open Balance','2014/08/01')
INSERT INTO [dbo].[Transaction] ([TransactionType],[AccountNumber],[DestAccount],[Amount],[Comment],[ModifyDate]) VALUES ('D',12,NULL,7000,'Open Balance','2014/08/01')
INSERT INTO [dbo].[Transaction] ([TransactionType],[AccountNumber],[DestAccount],[Amount],[Comment],[ModifyDate]) VALUES ('D',13,NULL,7500,'Open Balance','2014/08/01')
INSERT INTO [dbo].[Transaction] ([TransactionType],[AccountNumber],[DestAccount],[Amount],[Comment],[ModifyDate]) VALUES ('D',14,NULL,8000,'Open Balance','2014/08/01')
INSERT INTO [dbo].[Transaction] ([TransactionType],[AccountNumber],[DestAccount],[Amount],[Comment],[ModifyDate]) VALUES ('D',15,NULL,8500,'Open Balance','2014/08/01')
INSERT INTO [dbo].[Transaction] ([TransactionType],[AccountNumber],[DestAccount],[Amount],[Comment],[ModifyDate]) VALUES ('D',16,NULL,1500,'Open Balance','2014/08/01')
INSERT INTO [dbo].[Transaction] ([TransactionType],[AccountNumber],[DestAccount],[Amount],[Comment],[ModifyDate]) VALUES ('D',17,NULL,2000,'Open Balance','2014/08/01')
INSERT INTO [dbo].[Transaction] ([TransactionType],[AccountNumber],[DestAccount],[Amount],[Comment],[ModifyDate]) VALUES ('D',18,NULL,2500,'Open Balance','2014/08/01')
INSERT INTO [dbo].[Transaction] ([TransactionType],[AccountNumber],[DestAccount],[Amount],[Comment],[ModifyDate]) VALUES ('D',19,NULL,3000,'Open Balance','2014/08/01')
INSERT INTO [dbo].[Transaction] ([TransactionType],[AccountNumber],[DestAccount],[Amount],[Comment],[ModifyDate]) VALUES ('D',20,NULL,3500,'Open Balance','2014/08/01')
INSERT INTO [dbo].[Transaction] ([TransactionType],[AccountNumber],[DestAccount],[Amount],[Comment],[ModifyDate]) VALUES ('D',21,NULL,4000,'Open Balance','2014/08/01')
INSERT INTO [dbo].[Transaction] ([TransactionType],[AccountNumber],[DestAccount],[Amount],[Comment],[ModifyDate]) VALUES ('D',22,NULL,4500,'Open Balance','2014/08/01')
INSERT INTO [dbo].[Transaction] ([TransactionType],[AccountNumber],[DestAccount],[Amount],[Comment],[ModifyDate]) VALUES ('D',23,NULL,5000,'Open Balance','2014/08/01')
INSERT INTO [dbo].[Transaction] ([TransactionType],[AccountNumber],[DestAccount],[Amount],[Comment],[ModifyDate]) VALUES ('D',24,NULL,5500,'Open Balance','2014/08/01')
INSERT INTO [dbo].[Transaction] ([TransactionType],[AccountNumber],[DestAccount],[Amount],[Comment],[ModifyDate]) VALUES ('D',25,NULL,6000,'Open Balance','2014/08/01')
INSERT INTO [dbo].[Transaction] ([TransactionType],[AccountNumber],[DestAccount],[Amount],[Comment],[ModifyDate]) VALUES ('D',26,NULL,6500,'Open Balance','2014/08/01')
INSERT INTO [dbo].[Transaction] ([TransactionType],[AccountNumber],[DestAccount],[Amount],[Comment],[ModifyDate]) VALUES ('D',27,NULL,7000,'Open Balance','2014/08/01')
INSERT INTO [dbo].[Transaction] ([TransactionType],[AccountNumber],[DestAccount],[Amount],[Comment],[ModifyDate]) VALUES ('D',28,NULL,7500,'Open Balance','2014/08/01')
INSERT INTO [dbo].[Transaction] ([TransactionType],[AccountNumber],[DestAccount],[Amount],[Comment],[ModifyDate]) VALUES ('D',29,NULL,8000,'Open Balance','2014/08/01')
INSERT INTO [dbo].[Transaction] ([TransactionType],[AccountNumber],[DestAccount],[Amount],[Comment],[ModifyDate]) VALUES ('D',30,NULL,8500,'Open Balance','2014/08/01')
GO


/**CREATE Address**/
INSERT INTO [dbo].[Address] ([Street],[City],[State],[PostCode],[Phone]) VALUES ('71 Street St','Eagle Vale','NSW',2558,'966 7510');
INSERT INTO [dbo].[Address] ([Street],[City],[State],[PostCode],[Phone]) VALUES ('72 Street St','Eagleton','NSW',2324,'966 7511');
INSERT INTO [dbo].[Address] ([Street],[City],[State],[PostCode],[Phone]) VALUES ('73 Street St','Earlwood','NSW',2206,'966 7512');
INSERT INTO [dbo].[Address] ([Street],[City],[State],[PostCode],[Phone]) VALUES ('74 Street St','East Albury','NSW',2640,'966 7513');
INSERT INTO [dbo].[Address] ([Street],[City],[State],[PostCode],[Phone]) VALUES ('713 Street St','Macclesfield','VIC',3782,'966 7522');
INSERT INTO [dbo].[Address] ([Street],[City],[State],[PostCode],[Phone]) VALUES ('714 Street St','Macedon','VIC',3440,'966 7523');
INSERT INTO [dbo].[Address] ([Street],[City],[State],[PostCode],[Phone]) VALUES ('716 Street St','Macleod','VIC',3085,'966 7525');
INSERT INTO [dbo].[Address] ([Street],[City],[State],[PostCode],[Phone]) VALUES ('725 Street St','Jamboree Heights','QLD',4074,'966 7534');
INSERT INTO [dbo].[Address] ([Street],[City],[State],[PostCode],[Phone]) VALUES ('726 Street St','James Cook University','QLD',4811,'966 7535');
INSERT INTO [dbo].[Address] ([Street],[City],[State],[PostCode],[Phone]) VALUES ('727 Street St','Jandowae','QLD',4410,'966 7536');
GO

/**CREATE Payee**/
INSERT INTO [dbo].[Payee] ([PayeeName] ,[AddressID]) VALUES ('Internet',11)
INSERT INTO [dbo].[Payee] ([PayeeName] ,[AddressID]) VALUES ('Water',12)
INSERT INTO [dbo].[Payee] ([PayeeName] ,[AddressID]) VALUES ('Gas',13)
INSERT INTO [dbo].[Payee] ([PayeeName] ,[AddressID]) VALUES ('Electricity',14)
INSERT INTO [dbo].[Payee] ([PayeeName] ,[AddressID]) VALUES ('Mobile',15)
INSERT INTO [dbo].[Payee] ([PayeeName] ,[AddressID]) VALUES ('Mortgage',16)
INSERT INTO [dbo].[Payee] ([PayeeName] ,[AddressID]) VALUES ('Study',17)
INSERT INTO [dbo].[Payee] ([PayeeName] ,[AddressID]) VALUES ('Holiday Plan',18)
INSERT INTO [dbo].[Payee] ([PayeeName] ,[AddressID]) VALUES ('Rent',19)
INSERT INTO [dbo].[Payee] ([PayeeName] ,[AddressID]) VALUES ('Credit Card',20)
GO

/**Create BillPay**/
SET DATEFORMAT YMD
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (1,3,625.14,'2014/09/01',1,'Q',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (1,8,483.6,'2014/12/24',1,'Q',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (1,5,483.26,'2014/12/12',1,'O',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (1,4,432.34,'2014/09/15',1,'M',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (1,9,285.97,'2014/09/28',1,'O',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (1,7,50.76,'2014/10/17',1,'O',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (1,8,466.21,'2014/12/13',1,'O',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (2,8,111.73,'2014/11/20',1,'Q',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (2,3,675.96,'2014/11/23',1,'M',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (2,6,146.36,'2014/12/05',1,'M',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (2,5,208.73,'2014/09/23',1,'Y',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (2,9,810.91,'2014/09/20',1,'Q',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (2,7,713.17,'2014/12/05',1,'Q',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (2,4,71.16,'2014/10/05',1,'Y',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (3,2,916.3,'2014/10/28',1,'O',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (3,8,176.49,'2014/09/06',1,'M',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (3,8,885.84,'2014/09/23',1,'Y',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (3,3,223.44,'2014/12/28',1,'O',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (3,6,613.04,'2014/11/25',1,'Q',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (3,7,78.13,'2014/12/19',1,'Q',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (3,7,618.86,'2014/10/02',1,'Y',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (4,1,160.07,'2014/11/01',1,'Q',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (4,9,400.46,'2014/11/03',1,'Y',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (4,2,172.45,'2014/11/14',1,'O',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (4,2,672.47,'2014/11/08',1,'Q',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (4,10,633.04,'2014/10/31',1,'Q',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (4,6,462.59,'2014/10/12',1,'Q',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (4,2,172.93,'2014/11/20',1,'Q',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (5,2,823.99,'2014/09/25',1,'M',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (5,2,43.59,'2014/11/24',1,'Q',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (5,3,459.72,'2014/09/02',1,'Q',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (5,5,334.56,'2014/09/11',1,'Q',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (5,6,981.06,'2014/11/23',1,'M',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (5,9,596.56,'2014/09/14',1,'Q',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (5,10,640.95,'2014/10/13',1,'M',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (6,2,399.05,'2014/11/17',1,'Q',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (6,6,886.03,'2014/11/17',1,'O',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (6,7,634.44,'2014/10/11',1,'Q',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (6,6,241.11,'2014/11/02',1,'Q',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (6,4,953.32,'2014/09/09',1,'M',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (6,7,224.21,'2014/11/14',1,'Q',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (6,3,735.32,'2014/10/19',1,'Q',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (7,5,871.41,'2014/12/27',1,'Q',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (7,8,515.09,'2014/12/01',1,'Y',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (7,10,711.42,'2014/11/16',1,'M',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (7,6,693.31,'2014/10/31',1,'M',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (7,6,520.02,'2014/12/23',1,'O',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (7,2,103.3,'2014/09/16',1,'M',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (7,5,419.98,'2014/09/05',1,'O',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (8,3,421.24,'2014/09/27',1,'M',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (8,5,285.86,'2014/11/06',1,'O',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (8,7,368.61,'2014/12/03',1,'M',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (8,2,410.61,'2014/09/27',1,'M',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (8,7,901.98,'2014/10/26',1,'M',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (8,2,780.66,'2014/11/05',1,'Q',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (8,3,177.72,'2014/11/29',1,'M',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (9,3,511.55,'2014/12/23',1,'M',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (9,7,760.56,'2014/11/14',1,'O',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (9,1,268.08,'2014/09/29',1,'Q',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (9,10,41.95,'2014/10/06',1,'Y',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (9,1,865.84,'2014/11/25',1,'M',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (9,9,994.06,'2014/11/22',1,'Y',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (9,5,815.37,'2014/11/19',1,'Q',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (10,10,78.87,'2014/12/19',1,'M',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (10,4,171.37,'2014/11/12',1,'Q',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (10,9,784.81,'2014/12/04',1,'M',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (10,4,593.37,'2014/12/09',1,'M',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (10,6,257.54,'2014/12/01',1,'M',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (10,7,626.1,'2014/12/13',1,'Q',GETDATE())
INSERT INTO [dbo].[BillPay] ([AccountNumber],[PayeeID],[Amount],[ScheduleDate],[Status],[Period],[ModifyDate]) VALUES (10,9,728.49,'2014/09/14',1,'Y',GETDATE())

GO