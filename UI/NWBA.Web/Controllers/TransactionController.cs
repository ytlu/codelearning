﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NWBA.Common;
using NWBA.Domain.Entity;
using NWBA.Domain.Interface;
using NWBA.Logic.Interface.Service;
using NWBA.Web.Logic;
using NWBA.Web.Models;

namespace NWBA.Web.Controllers
{
    [Authorize(Roles = "User")]
    public class TransactionController : BaseController
    {
        #region Private Members

        private readonly IAccountService _accountService;
        private readonly IPayeeService _payeeService;
        private readonly IBillPayService _billPayService;

        #endregion


        #region Constructor

        public TransactionController(ICustomerService customerService, IAccountService accountService,
            IPayeeService payeeService, IBillPayService billPayService)
            : base(customerService)
        {
            _accountService = accountService;
            _payeeService = payeeService;
            _billPayService = billPayService;
        }

        #endregion


        #region Action Results

        /// <summary>
        /// This action is the ATM function
        /// </summary>
        /// <returns>ATM page for customer to submit transaction</returns>
        public ActionResult Manage()
        {
            var model = new TransactionModel();

            // Get the available accounts for customer to select from
            model.AvailableAccounts = _accountService.GetCustomerAccounts(GetCustomerID);
            return View(model);
        }

        [HttpPost]
        public ActionResult Manage(TransactionModel model)
        {
            try
            {
                // Make sure customer owns the account
                if (!IsCustomerAccount(model.AccountNumber))
                {
                    // The error message should be ambiguous to avoid hacker using the detail to figure available account on system
                    throw new Exception("Invalid account selected");
                }

                // Process transaction
                var transactionResult = _accountService.SaveTransaction(model);

                // Store the result to session, so when customer refresh, it won't make another transaction
                Session[SiteConfig.SessionConfig.TransactionReceiptKey] = transactionResult;
                return RedirectToAction("TransactionResult");
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", e.Message);
            }

            model.AvailableAccounts = _accountService.GetCustomerAccounts(GetCustomerID);
            return View(model);
        }

        public ActionResult TransactionResult()
        {
            ITransactionResult transactionResult =
                Session[SiteConfig.SessionConfig.TransactionReceiptKey] as ITransactionResult;

            // When customer refresh, it should take them to ATM page
            if (transactionResult == null)
            {
                return RedirectToAction("Manage");
            }

            // The result should be clear as soon as it's been read
            Session.Remove(SiteConfig.SessionConfig.TransactionReceiptKey);
            return View(transactionResult);
        }

        public ActionResult PayBill()
        {
            var model = new ScheduleBillPayModel
            {
                IsValidRecord = true,
                AvailableAccounts = _accountService.GetCustomerAccounts(GetCustomerID),
                Payees = _payeeService.GetPayess()
            };

            return View(model);
        }

        public ActionResult EditBill(int id)
        {
            ScheduleBillPayModel model;

            try
            {
                // Attempt to load bill pay record
                model = DataService.ConvertToObjectInterface<IBillPay, ScheduleBillPayModel>(
                    _billPayService.GetBillPay(GetCustomerID, id));

                model.IsValidRecord = true;
                model.AvailableAccounts = _accountService.GetCustomerAccounts(GetCustomerID);
                model.Payees = _payeeService.GetPayess();

                // Setup display message, this page is redirect after
                // customer update the pay bill
                if (IsPayBillCreated())
                {
                    model.ActionMsg = "Pay Bill Created";
                }

                if (IsPayBillDeletedError())
                {
                    ModelState.AddModelError("", "Error deleting payment");
                }
            }
            catch (Exception e)
            {
                model = new ScheduleBillPayModel {ErrorMsg = e.Message, IsValidRecord = false};
            }


            return View("PayBill", model);
        }

        [HttpPost]
        public ActionResult EditBill(ScheduleBillPayModel model)
        {
            model.IsValidRecord = true;

            try
            {
                bool isNewRecord = (model.BillPayID == 0);

                // Make sure customer owns the account
                if (!IsCustomerAccount(model.AccountNumber))
                {
                    // The error message should be ambiguous to avoid hacker using the detail to figure available account on system
                    throw new Exception("Invalid account selected");
                }

                Session[SiteConfig.SessionConfig.BillPaySavedKey] = true;
                int billPayID = _billPayService.SaveBillPay(model);

                if (isNewRecord)
                {
                    return RedirectToAction("EditBill", new { id = billPayID });
                }

                model.ActionMsg = "Pay Bill Updated";
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", e.Message);
            }

            model.AvailableAccounts = _accountService.GetCustomerAccounts(GetCustomerID);
            model.Payees = _payeeService.GetPayess();

            return View("PayBill", model);
        }


        public ActionResult ViewBillList()
        {
            return View(GetBillingModel(1));
        }

        public ActionResult GetBillingList(int pageNumber)
        {
            return PartialView(GetBillingModel(pageNumber));
        }

        public ActionResult DeletePayment(int id)
        {
            try
            {
                _billPayService.DeleteBillPay(GetCustomerID, id);
                return RedirectToAction("ViewBillList");
            }
            catch
            {
                Session[SiteConfig.SessionConfig.BillPayDeletedErrorKey] = true;
                return EditBill(id);
            }
        }

        #endregion


        #region Helper Methods

        /// <summary>
        /// Build the billing model with paging
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <returns>Billing model</returns>
        private BillingModel GetBillingModel(int pageNumber)
        {
            BillingModel model;

            try
            {
                model =
                    DataService.ConvertToObjectInterface<IBillPayRecord, BillingModel>(
                        _customerService.GetBillPays(GetCustomerID, pageNumber));

                // Build the paging model if there is record
                if (model.HasRecord)
                {
                    model.PagingModel = new PagingModel()
                    {
                        ActionName = "GetBillingList",
                        ControllerName = "Transaction",
                        ID = "",
                        PageModel = model.PageModel
                    };
                }
            }
            catch (Exception ex)
            {
                model = new BillingModel { ErrorMsg = ex.Message };
            }
            return model;
        }


        /// <summary>
        /// This method is to help to identify a bill record has been created
        /// </summary>
        /// <returns>True if bill record has been created</returns>
        private bool IsPayBillCreated()
        {
            bool paybillCreated = false;
            if (Session[SiteConfig.SessionConfig.BillPaySavedKey] != null)
            {
                paybillCreated = true;
                Session.Remove(SiteConfig.SessionConfig.BillPaySavedKey);
            }

            return paybillCreated;
        }

        /// <summary>
        /// This method is to help to identify a bill record has been delete
        /// </summary>
        /// <returns>True if bill record has been deleted</returns>
        private bool IsPayBillDeletedError()
        {
            bool paybillDeleteError= false;
            if (Session[SiteConfig.SessionConfig.BillPayDeletedErrorKey] != null)
            {
                paybillDeleteError = true;
                Session.Remove(SiteConfig.SessionConfig.BillPayDeletedErrorKey);
            }

            return paybillDeleteError;
        }

        #endregion
    }
}