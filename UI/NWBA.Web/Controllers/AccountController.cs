﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI;
using Microsoft.Ajax.Utilities;
using NWBA.Common;
using NWBA.Domain.Interface;
using NWBA.Logic.Interface.Service;
using NWBA.Web.Logic;
using NWBA.Web.Models;

namespace NWBA.Web.Controllers
{
    [Authorize(Roles = "User")]
    public class AccountController : BaseController
    {
        #region Private Members

        private readonly IAccountService _accountService;
        private readonly IAddressService _addressService;
        private readonly ILoginService _loginService;

        #endregion


        #region Constructor

        public AccountController(ICustomerService customerService, IAccountService accountService,
            IAddressService addressService, ILoginService loginService)
            : base(customerService)
        {
            _accountService = accountService;
            _addressService = addressService;
            _loginService = loginService;
        }

        #endregion


        #region Action Results

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model, string returnUrl)
        {

            if (ModelState.IsValid && Membership.ValidateUser(model.UserName, model.Password))
            {
                FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);

                // Check if the user is administrator and redirect accordingly
                if (_loginService.IsAdministrator(model.UserName))
                {
                    return RedirectToAction("Index", "Admin");
                }
                else
                {
                    SetCustomerDetailToSession();
                    return RedirectToLocal(returnUrl);    
                }
            }

            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("", "The user name or password provided is incorrect.");
            return View(model);
        }

        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Account");
        }

        public ActionResult MyStatement()
        {
            return View(_accountService.GetCustomerAccounts(GetCustomerID));
        }

        /// <summary>
        /// Return the transaction list for the account, this intent for AJAX call only
        /// </summary>
        /// <param name="id">Account number</param>
        /// <param name="pageNumber">Page number</param>
        /// <returns></returns>
        public ActionResult GetStatementTransaction(int id, int pageNumber)
        {
            var model = new AccountStatmentModel()
            {
                IsValidAccount = false
            };

            // Verify if the account belong to the customer
            if (IsCustomerAccount(id))
            {
                model = NWBAModelBuilder.BuildAccountStatementModel(id,
                    pageNumber, "GetStatementTransaction", "Account", _accountService);
            }
            else
            {
                // Do not enclose too much information to the user, to prevent user try to find active account
                model.ErrorMsg = "Invalid account #" + id.ToString();
            }

            return PartialView("_GetStatementTransaction", model);
        }

        public ActionResult MyProfile()
        {
            ProfileModel profileModel;

            try
            {
                profileModel = NWBAModelBuilder.BuildCustomerProfile(GetCustomerID, _customerService);
            }
            catch (Exception e)
            {
                profileModel = new ProfileModel()
                {
                    ValidProfile = false
                };
            }

            return View(profileModel);
        }

        [HttpPost]
        public ActionResult MyProfile(ProfileModel model)
        {
            try
            {
                // The customer ID and address ID is loaded from server side, 
                // we do not want user to have option to modify the ID in hidden fields
                model.CustomerID = GetCustomerID;
                model.ValidProfile = true;
                var address = _customerService.GetAddressByCustomerID(GetCustomerID);
                model.AddressID = address.AddressID;

                _addressService.SaveAddress(model);
                _customerService.SaveCustomer(model);

                model.ProfileUpdated = true;
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", e.Message);
            }

            return View(model);
        }


        public ActionResult ChangePassword()
        {
            var model = new ChangePasswordModel {PasswordUpdated = IsPasswordUpdated()};
            return View(model);
        }

        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel changePasswordModel)
        {
            try
            {
                _customerService.UpdatePassword(GetCustomerID, User.Identity.Name, changePasswordModel.Password,
                    changePasswordModel.NewPassword);
                Session[SiteConfig.SessionConfig.PasswordUpdatedKey] = true;
                return RedirectToAction("ChangePassword");
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", e.Message);
            }

            return View(changePasswordModel);
        }

        #endregion


        #region Helper Methods

        private ActionResult RedirectToLocal(string returnUrl)
        {
            return RedirectToAction("Manage", "Transaction");

            // Usually, after user login, they should be redirect to the page they try to access
            // However due to requirement of the project, it will always redirect to ATM page
            /*
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Manage", "Transaction");
            }*/
        }

        /// <summary>
        /// This method check if the password has been updated
        /// This is require to stored in section as the update password page should redirect
        /// after password been updated sucessfully
        /// </summary>
        /// <returns>True if the password has been updated</returns>
        private bool IsPasswordUpdated()
        {
            bool updatedPassword = false;

            // Attemp to read the seesion key and remove it if it's been found
            if (Session[SiteConfig.SessionConfig.PasswordUpdatedKey] != null)
            {
                updatedPassword = true;
                Session.Remove(SiteConfig.SessionConfig.PasswordUpdatedKey);
            }

            return updatedPassword;
        }

        #endregion
    }
}