﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using NWBA.Logic.Interface.Service;
using NWBA.Web.Logic;

namespace NWBA.Web.Controllers
{
    /// <summary>
    /// This class contains the required function for a logon customer, all the controller which require access to 
    /// customer information should inherit from this class.
    /// </summary>
    [Authorize]
    public abstract class BaseController : Controller
    {
        #region Private Members

        private bool _hasLoadCustomer = false;
        private int _customerID = 0;
        private string _customerAccountList = "";

        #endregion


        #region Protected Member

        protected readonly ICustomerService _customerService;

        protected int GetCustomerID
        {
            get
            {
                EnsureLoadCustomer();
                return _customerID;
            }
        }

        #endregion


        #region Constructor

        protected BaseController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        #endregion


        #region Protected Methods

        /// <summary>
        /// Check if the account belong to the customer
        /// </summary>
        /// <param name="accountNumber">Account number which customer try to look up</param>
        /// <returns>True if the account belong to customer, false otherwise</returns>
        protected bool IsCustomerAccount(int accountNumber)
        {
            EnsureLoadCustomer();
            return _customerAccountList.IndexOf(string.Format("|{0}|", accountNumber)) > -1;
        }

        /// <summary>
        /// Push customer detail to session with the customer user name
        /// </summary>
        protected void SetCustomerDetailToSession()
        {
            // Verify if the user is authenticated first
            if (User.Identity.IsAuthenticated)
            {
                // Check if any of the session key is missing
                if (Session[SiteConfig.SessionConfig.CustomerIDKey] == null || Session[SiteConfig.SessionConfig.CustomerAccountListKey] == null)
                {
                    // Set customer ID
                    try
                    {
                        _customerID = _customerService.GetCustomerIDByUserName(User.Identity.Name);
                        Session[SiteConfig.SessionConfig.CustomerIDKey] = _customerID;

                        // Set customer account list, separator by |
                        var _customerAccountList = "|" + string.Join("|",
                            _customerService.GetCustomerAccountNumberList(_customerID).Select(acc => acc.ToString())) + "|";

                        Session[SiteConfig.SessionConfig.CustomerAccountListKey] = _customerAccountList;
                    }
                    catch (Exception ex)
                    {
                        // Force sign out of customer if they have tried to hack the session with invalid customer
                        FormsAuthentication.SignOut();
                    }
                }
                else
                {
                    // Load the customer detail from session to avoid unnecessary database call
                    _customerID = Convert.ToInt32(Session[SiteConfig.SessionConfig.CustomerIDKey]);
                    _customerAccountList = Session[SiteConfig.SessionConfig.CustomerAccountListKey].ToString();
                }
            }
        }

        #endregion


        #region Helper Methods

        /// <summary>
        /// This method ensure the loading of the customer only happen once.
        /// </summary>
        private void EnsureLoadCustomer()
        {
            if (!_hasLoadCustomer)
            {
                SetCustomerDetailToSession();
                _hasLoadCustomer = true;
            }
        }

        #endregion


    }
}
