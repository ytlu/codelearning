﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using NWBA.Domain.Entity;
using NWBA.Logic.Interface.Service;
using NWBA.Web.Logic;
using NWBA.Web.Models;

namespace NWBA.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        #region Private Members

        private readonly IAdministrationService _administrationService;
        private readonly ICustomerService _customerService;
        private readonly IAccountService _accountService;

        #endregion


        #region Constructor

        public AdminController(IAdministrationService administrationService, ICustomerService customerService,
            IAccountService accountService)
        {
            _administrationService = administrationService;
            _customerService = customerService;
            _accountService = accountService;
        }

        #endregion


        #region Action Results

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CustomerList()
        {
            return View(NWBAModelBuilder.GetCustomListModel(1, _administrationService));
        }

        public ActionResult GetCustomerListPage(int pageNumber)
        {
            return PartialView(NWBAModelBuilder.GetCustomListModel(pageNumber, _administrationService));
        }

        public ActionResult ViewCustomer(int id)
        {
            var model = new CustomerAccountModel();
            ProfileModel profileModel;

            try
            {
                profileModel = NWBAModelBuilder.BuildCustomerProfile(id, _customerService);
                model.CustomerAccounts = _accountService.GetCustomerAccounts(id);
            }
            catch (Exception e)
            {
                profileModel = new ProfileModel()
                {
                    ValidProfile = false
                };
            }

            model.CustomerProfile = profileModel;

            return View(model);
        }

        public ActionResult GetCustomerStatement(int id, int pageNumber)
        {
            return PartialView("_GetStatementTransaction",
                NWBAModelBuilder.BuildAccountStatementModel(id, pageNumber,
                    "GetCustomerStatement", "Admin", _accountService));
        }

        public ActionResult ManagePayment()
        {
            return View(NWBAModelBuilder.GetAdminBillPayModel(1, _administrationService));
        }

        public ActionResult GetPaymentListPage(int pageNumber)
        {
            return PartialView(NWBAModelBuilder.GetAdminBillPayModel(pageNumber, _administrationService));
        }

        [HttpPost]
        public JsonResult PausePayment(int id)
        {
            return SetBillPaymentStatus(id, false);
        }

        [HttpPost]
        public JsonResult EnablePayment(int id)
        {
            return SetBillPaymentStatus(id, true);
        }

        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Account");
        }

        #endregion


        #region Helper Methods

        /// <summary>
        /// Set the payment status and return the result in JSON
        /// </summary>
        /// <param name="id">Bill pay ID</param>
        /// <param name="isEnable">True if enabling payment</param>
        /// <returns>JSON result of the updated operation</returns>
        public JsonResult SetBillPaymentStatus(int id, bool isEnable)
        {
            // Configure value for the update link URL
            var sucessfulSet = true;
            var newUrl = Url.Action((isEnable ? "PausePayment" : "EnablePayment"), "Admin", new { id });
            var urlText = (isEnable ? "Pause Payment" : "Enable Payment");
            var errorMsg = string.Empty;

            try
            {
                // Attemp to call database to update the bill pay status
                if (isEnable)
                {
                    _administrationService.EnableBillPay(id);
                }
                else
                {
                    _administrationService.PauseBillPay(id);    
                }
            }
            catch (Exception e)
            {
                sucessfulSet = false;
                errorMsg = e.Message;
            }

            return Json(new { sucessfulSet, errorMsg, newUrl, urlText });
        }

        #endregion

    }
}
