﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using NWBA.Web.App_Start;

namespace NWBA.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            // Setup DI container and AutoMapper
            var bootstrap = new Bootstrap();
            bootstrap.Configure();
            bootstrap.ConfigureMapper();

            // Setup SimpleMembership
            //GlobalFilters.Filters.Add(new InitializeSimpleMembershipAttribute());

            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();

#if !DEBUG
            log4net.Config.XmlConfigurator.Configure();
#endif
        }

        protected void MvcApplication_PostAuthenticateRequest(object sender, EventArgs e)
        {
            HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];

            if (authCookie != null)
            {
                FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                var identity = new GenericIdentity(authTicket.Name, "Forms");

                // Setup custom role for the system
                var userRoles = Roles.GetRolesForUser(identity.Name);
                var principal = new GenericPrincipal(identity, userRoles);
                Context.User = principal;
            }
        }
    }
}