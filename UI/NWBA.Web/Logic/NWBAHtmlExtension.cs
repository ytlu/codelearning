﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Antlr.Runtime;
using NWBA.Domain.Interface;

namespace NWBA.Web.Logic
{
    /// <summary>
    /// This class is HTML helper extension for the MVC pages
    /// </summary>
    public static class NWBAHtmlExtension
    {

        #region Static Extension Methods

        /// <summary>
        /// Format credit string for display as currency
        /// </summary>
        /// <param name="amount">Amount to be display</param>
        /// <returns>Currency string if amount is positive</returns>
        public static string CreditString(this decimal amount)
        {
            return (amount < 0 ? "" : amount.ToString("C"));
        }

        /// <summary>
        /// Format debit string for display as currency
        /// </summary>
        /// <param name="amount">Amount to be display</param>
        /// <returns>Currency string if amount is negative</returns>
        public static string DebitString(this decimal amount)
        {
            return (amount < 0 ? amount.ToString("C") : "");
        }

        /// <summary>
        /// Format datetime string for display as dd/MM/yy hh:mm:ss
        /// </summary>
        /// <param name="dateValue">Date value to be formatted</param>
        /// <returns>Formatted datetime string</returns>
        public static string DateDisplay(this DateTime dateValue)
        {
            return dateValue.ToString("dd/MM/yy hh:mm:ss");
        }

        /// <summary>
        /// Format datetime string for display as dd/MM/yy
        /// </summary>
        /// <param name="dateValue">Date value to be formatted</param>
        /// <returns>Formatted datetime string without time</returns>
        public static string DateOnlyDisplay(this DateTime dateValue)
        {
            return dateValue.ToString("dd/MM/yy");
        }

        /// <summary>
        /// Build dropdown list for the transaction type
        /// </summary>
        /// <param name="helper">HtmlHelper</param>
        /// <param name="selectedValue">Selected transaction type</param>
        /// <returns></returns>
        public static SelectList BuildTransactionType(this HtmlHelper helper, string selectedValue)
        {
            var items = new List<SelectListItem>
            {
                new SelectListItem()
                {
                    Text = "Select transaction type",
                    Value = ""
                },
                new SelectListItem()
                {
                    Text = "Credit",
                    Value = "D",
                    Selected = (selectedValue == "D")
                },
                new SelectListItem()
                {
                    Text = "Withdrawal",
                    Value = "W",
                    Selected = (selectedValue == "W")
                },
                new SelectListItem()
                {
                    Text = "Transfer",
                    Value = "T",
                    Selected = (selectedValue == "T")
                }
            };


            return new SelectList(items, "Value", "Text");
        }

        /// <summary>
        /// Build account list dropdown
        /// </summary>
        /// <param name="helper">HtmlHelper</param>
        /// <param name="accountList">Account list belong to the customer</param>
        /// <param name="accountNumber">Selected account number</param>
        /// <returns></returns>
        public static SelectList BuildAccountList(this HtmlHelper helper, IList<IAccount> accountList, int accountNumber)
        {
            var items = new List<SelectListItem>
            {
                new SelectListItem()
                {
                    Text = "Select account",
                    Value = "0"
                }
            };

            items.AddRange(accountList.Select(account => new SelectListItem()
            {
                Text = string.Format("{0} - Acc #{1}, ({2})", account.AccountTypeDisplay,
                    account.AccountNumber, account.Balance.ToString("C")),
                Value = account.AccountNumber.ToString(),
                Selected = (account.AccountNumber == accountNumber)
            }));

            return new SelectList(items, "Value", "Text");
        }

        /// <summary>
        /// Build payee list dropdown
        /// </summary>
        /// <param name="helper">HtmlHelper</param>
        /// <param name="payeeList">Shared payee list</param>
        /// <param name="payeeID">Selected payee</param>
        /// <returns>Payee list for dropdown</returns>
        public static SelectList BuildPayeeList(this HtmlHelper helper, IList<IPayee> payeeList, int payeeID)
        {
            var items = new List<SelectListItem>
            {
                new SelectListItem()
                {
                    Text = "Select payee",
                    Value = "0"
                }
            };

            items.AddRange(payeeList.Select(payee => new SelectListItem()
            {
                Text = payee.PayeeName,
                Value = payee.PayeeID.ToString(),
                Selected = (payee.PayeeID == payeeID)
            }));

            return new SelectList(items, "Value", "Text");
        }

        /// <summary>
        /// Build dropdown list for the period type
        /// </summary>
        /// <param name="helper">HtmlHelper</param>
        /// <param name="selectedValue">Selected period type</param>
        /// <returns>Period list for dropdown</returns>
        public static SelectList BuildPeriodList(this HtmlHelper helper, string selectedValue)
        {
            var items = new List<SelectListItem>
            {
                new SelectListItem()
                {
                    Text = "Select period",
                    Value = ""
                },
                new SelectListItem()
                {
                    Text = "One time",
                    Value = "O",
                    Selected = (selectedValue == "O")
                },
                new SelectListItem()
                {
                    Text = "Monthly",
                    Value = "M",
                    Selected = (selectedValue == "M")
                },
                new SelectListItem()
                {
                    Text = "Quarterly",
                    Value = "Q",
                    Selected = (selectedValue == "Q")
                },
                new SelectListItem()
                {
                    Text = "Yearly",
                    Value = "Y",
                    Selected = (selectedValue == "Y")
                }

            };


            return new SelectList(items, "Value", "Text");
        }

        #endregion

    }
}