﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NWBA.Common;
using NWBA.Domain.Entity;
using NWBA.Domain.Interface;
using NWBA.Logic.Interface.Service;
using NWBA.Web.Models;

namespace NWBA.Web.Logic
{
    /// <summary>
    /// This class contain the builder for the models
    /// </summary>
    internal class NWBAModelBuilder
    {

        #region Internal Statis Methods

        /// <summary>
        /// Build customer list model
        /// </summary>
        /// <param name="pageNumber">Selected customer list page</param>
        /// <param name="administrationService">Admin serivce to retrieve the data from</param>
        /// <returns>Customer list model for the view</returns>
        internal static CustomListModel GetCustomListModel(int pageNumber, IAdministrationService administrationService)
        {
            var model = new CustomListModel()
            {
                IsValidAccount = true
            };

            var customerList = administrationService.GetCustomerList(pageNumber);

            model.HasCustomer = (customerList.HasCustomer && customerList.Customers.Any());

            // Populate customer paging model
            if (model.HasCustomer)
            {
                model.PagingModel = new PagingModel()
                {
                    ActionName = "GetCustomerListPage",
                    ControllerName = "Admin",
                    ID = "",
                    PageModel = customerList.PageModel
                };

                model.CustomerList = customerList.Customers;
            }

            return model;
        }

        /// <summary>
        /// Build customer profile model
        /// </summary>
        /// <param name="customerID">Customer ID to retrieve the record</param>
        /// <param name="customerService">Customer service to retrieve the data from</param>
        /// <returns>Profile model for the view</returns>
        internal static ProfileModel BuildCustomerProfile(int customerID, ICustomerService customerService)
        {
            ProfileModel profileModel;
            var customer = customerService.GetCustomerByID(customerID);

            // Attempt to load customer detail
            profileModel =
                DataService.ConvertToObjectInterface<IAddress, ProfileModel>(
                    customerService.GetAddressByCustomerID(customerID));

            profileModel.CustomerName = customer.CustomerName;
            profileModel.TFN = customer.TFN;
            profileModel.CustomerID = customer.CustomerID;

            profileModel.ValidProfile = true;

            return profileModel;
        }

        /// <summary>
        /// Build account statment model
        /// </summary>
        /// <param name="accountNumber">Account number to look up</param>
        /// <param name="pageNumber">Selected page</param>
        /// <param name="actionName">Action name for the paging URL</param>
        /// <param name="controllerName">Controller name for the paging URL</param>
        /// <param name="accountService">Acount service to retrieve data from</param>
        /// <returns>Account statment model for the view</returns>
        internal static AccountStatmentModel BuildAccountStatementModel(int accountNumber, int pageNumber,
            string actionName, string controllerName, IAccountService accountService)
        {
            var model = new AccountStatmentModel()
            {
                IsValidAccount = false
            };

            try
            {
                // Attempt to load the account details for customer
                var accTransactions = accountService.GetAccountTransactions(accountNumber, pageNumber);
                model.IsValidAccount = true;
                model.HasTransaction = (accTransactions.HasTransaction && accTransactions.Transactions.Any());

                // Populate transaction list
                if (model.HasTransaction)
                {
                    model.PageModel = new PagingModel()
                    {
                        ActionName = actionName,
                        ControllerName = controllerName,
                        ID = accountNumber.ToString(),
                        PageModel = accTransactions.PageModel
                    };

                    model.Transactions = accTransactions.Transactions;
                }
            }
            catch (Exception ex)
            {
                model.ErrorMsg = ex.Message;
            }

            return model;
        }

        /// <summary>
        /// Build admin bill pay model
        /// </summary>
        /// <param name="pageNumber">Selected page</param>
        /// <param name="administrationService">Admin service to retrieve data from</param>
        /// <returns>Customer bill pay model for the view</returns>
        internal static CustomerBillPayModel GetAdminBillPayModel(int pageNumber,
            IAdministrationService administrationService)
        {
            var model = new CustomerBillPayModel();

            var billPays = administrationService.GetAdminBillPays(pageNumber);

            model.HasBillPay = (billPays.HasBillPay && billPays.BillPayRecords.Any());

            // Populate bill pay paging
            if (model.HasBillPay)
            {
                model.PagingModel = new PagingModel()
                {
                    ActionName = "GetPaymentListPage",
                    ControllerName = "Admin",
                    ID = "",
                    PageModel = billPays.PageModel
                };

                model.BillPayRecords = billPays.BillPayRecords;
            }


            return model;
        }

        #endregion

    }
}