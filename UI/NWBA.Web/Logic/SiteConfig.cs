﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace NWBA.Web.Logic
{
    public class SiteConfig
    {
        #region Private Static Members

        private static int? _defaultPageSize;

        #endregion


        #region Session Config Value

        /// <summary>
        /// This class contains all the session key value, all the sesison key should be centralise to this class
        /// </summary>
        public class SessionConfig
        {
            #region Internal Static String

            internal static string CustomerIDKey { get { return "SessionCustomerID"; } }
            internal static string CustomerAccountListKey { get { return "SessionCustomerAccountList"; } }
            internal static string TransactionReceiptKey { get { return "SessionTransactionReceiptKey"; } }
            internal static string PasswordUpdatedKey { get { return "SessionPasswordUpdatedKey"; } }
            internal static string BillPaySavedKey { get { return "SessionBillPaySavedKey"; } }
            internal static string BillPayDeletedErrorKey { get { return "BillPayDeletedErrorKey "; } }

            #endregion
        }

        #endregion

    }
}