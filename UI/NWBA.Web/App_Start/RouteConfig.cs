﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace NWBA.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            // Mapping for customer report
            routes.MapRoute(
                name: "AdminCustomerReport",
                url: "Admin/GetCustomerListPage/{pageNumber}",
                defaults: new {controller = "Admin", action = "GetCustomerListPage", pageNumber = 1}
                );

            // Mapping for admin bill pay
            routes.MapRoute(
                name: "AdminPaymentReport",
                url: "Admin/GetPaymentListPage/{pageNumber}",
                defaults: new {controller = "Admin", action = "GetPaymentListPage", pageNumber = 1}
                );

            // Mapping for general report with paging
            routes.MapRoute(
                name: "Report",
                url: "{controller}/{action}/{id}/{pageNumber}",
                defaults: new {controller = "Account", action = "GetStatementTransaction", pageNumber = 1}
                );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new {controller = "Home", action = "Index", id = UrlParameter.Optional}
                );
        }
    }
}