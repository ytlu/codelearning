﻿using System.Web;
using System.Web.Optimization;

namespace NWBA.Web
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/static/js/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                "~/static/js/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                "~/static/js/jquery.unobtrusive*",
                "~/static/js/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/mystatement").Include(
                "~/static/js/my-statement.js"));

            bundles.Add(new ScriptBundle("~/bundles/atmtransaction").Include(
                "~/static/js/atm-transaction.js"));

            bundles.Add(new ScriptBundle("~/bundles/paybill").Include(
                "~/static/js/paybill.js"));


            bundles.Add(new ScriptBundle("~/bundles/pagingpanel").Include(
                "~/static/js/paging-panel.js"));

            bundles.Add(new ScriptBundle("~/bundles/managepayment").Include(
                "~/static/js/manage-payment.js"));


            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                "~/static/js/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/headerjs").Include("~/static/js/bootstrap.js"));

            bundles.Add(new StyleBundle("~/static/css").Include(
                "~/Static/CSS/themes/base/jquery-ui.css",
                "~/Static/CSS/bootstrap.css",
                "~/Static/CSS/bootstrap-theme.css",
                "~/static/css/Site.css"));

            bundles.Add(new StyleBundle("~/static/admincss").Include(
                "~/static/css/SiteAdmin.css"));

            bundles.Add(new StyleBundle("~/static/css/themes/base/css").Include(
                "~/static/css/themes/base/jquery.ui.core.css",
                "~/static/css/themes/base/jquery.ui.resizable.css",
                "~/static/css/themes/base/jquery.ui.selectable.css",
                "~/static/css/themes/base/jquery.ui.accordion.css",
                "~/static/css/themes/base/jquery.ui.autocomplete.css",
                "~/static/css/themes/base/jquery.ui.button.css",
                "~/static/css/themes/base/jquery.ui.dialog.css",
                "~/static/css/themes/base/jquery.ui.slider.css",
                "~/static/css/themes/base/jquery.ui.tabs.css",
                "~/static/css/themes/base/jquery.ui.datepicker.css",
                "~/static/css/themes/base/jquery.ui.progressbar.css",
                "~/static/css/themes/base/jquery.ui.theme.css"));
        }
    }
}