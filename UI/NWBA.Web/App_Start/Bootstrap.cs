﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using AutoMapper;
using NWBA.Domain.Interface;
using NWBA.Logic.DI;
using NWBA.Logic.Interface.Repository;
using NWBA.Logic.Interface.Service;
using NWBA.Logic.Repository;
using NWBA.Logic.Service;
using NWBA.Web.Models;

namespace NWBA.Web.App_Start
{
    /// <summary>
    /// This class manage the data mapping and dependency injection
    /// </summary>
    public class Bootstrap
    {
        #region Private Members

        private NWBALogicDIResolver _resolver;

        #endregion


        #region Public Methods

        /// <summary>
        /// This method manage the dependenecy injection
        /// </summary>
        public void Configure()
        {

            _resolver = new NWBALogicDIResolver(NWBALogicDIResolver.ResolverType.MVC);
            _resolver.ConfigureForWebApi(Assembly.GetExecutingAssembly());
            var resolver = new AutofacDependencyResolver(_resolver.myContainer);

            DependencyResolver.SetResolver(resolver);
        }

        /// <summary>
        /// This method manage the object mapping
        /// </summary>
        public void ConfigureMapper()
        {
            SetupObjectMap();
        }

        #endregion


        #region Helper Methods
        
        /// <summary>
        /// Setup mapping between objects
        /// </summary>
        private void SetupObjectMap()
        {
            Mapper.CreateMap<IAddress, ProfileModel>();
            Mapper.CreateMap<IBillPay, ScheduleBillPayModel>();
            Mapper.CreateMap<IBillPayRecord, BillingModel>();
        }

        #endregion
    }
}