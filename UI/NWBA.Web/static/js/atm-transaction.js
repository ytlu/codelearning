﻿(function () {

    $(document).ready(function () {

        // Show/hide destionation account depend on the transaction type
        $(".js-transaction-type").bind("change", function (e) {

            e.preventDefault();

            var $this = $(this);

            // Only show the dest account if the transaction type is transfer
            if ($this.val() == "T") {
                $(".js-dest-account").show();
            } else {
                $(".js-dest-account").hide();
            }
        });


        // Reset all form value and vlidation error
        $(".js-reset-transaction").bind("click", function (e) {
            e.preventDefault();

            var $form = $("#transaction-form");
            $form[0].reset();

            // Reset the form
            $form.validate().resetForm();

            // Clear out the validation message
            $form.find("[data-valmsg-summary=true]")
                .removeClass("validation-summary-errors")
                .addClass("validation-summary-valid")
                .find("ul").empty();

            $form.find("[data-valmsg-replace]")
                .removeClass("field-validation-error")
                .addClass("field-validation-valid")
                .empty();

            // Hide the destination account
            $(".js-dest-account").hide();
        });
    });

})();