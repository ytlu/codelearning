﻿$(function () {

    // Method to making AJAX call to change payment status
    var managePaymentStatus = function (callUrl, link) {

        // The AJAX request should be POST for any CRUD operation
        $.post(callUrl,
             function (data) {

                 // Check if the AJAX call has been made sucessfully
                 // If it's successful, change the link URL and text
                 if (data.sucessfulSet) {
                     link.attr("href", data.newUrl);
                     link.text(data.urlText);
                 } else {
                     alert("Error! " + data.errorMsg);
                 }
             }
         ).fail(function (jqXHR, textStatus, errorThrown) {
             alert(textStatus);
         });
    }

    // Bind the click event to the pause/enable link.
    // Need to use on method because the paging is done with AJAX
    $("#billpay-list").on("click", ".js-manage-payment-status a",
        function(e) {
            e.preventDefault();

            var $this = $(this);

            // Build the confirmation msg before pause/enable payment
            var confirmMsg = (/enable/gi.test($this.text())) ?
                "Do you want to enable this payment?" :
                "Do you want to pause this payment";

            // Confirm with user before proceed to update status
            if (confirm(confirmMsg)) {
                managePaymentStatus($this.attr("href"), $this);
            }
        }
    );
});
