﻿$(function ($) {

    // Overwrite default date valudation to AU date format
    $.validator.addMethod('date',
        function(value, element) {
            if (this.optional(element)) {
                return true;
            }

            var ok = true;
            try {
                $.datepicker.parseDate('dd/mm/yy', value);
            } catch (err) {
                ok = false;
            }
            return ok;
        }
    );
});

var dateToday = new Date();

$(function () {

    // Init date picker for the schedule
    $('.select-date').datepicker({
        dateFormat: 'dd/mm/yy',
        minDate: dateToday
    });


    // Reset all form value and vlidation error
    $(".js-reset-transaction").bind("click", function (e) {
        e.preventDefault();

        var $form = $("#billpay-form");

        // Reset the form
        $form[0].reset();
        $form.validate().resetForm();

        // Clear out the validation message
        $form.find("[data-valmsg-summary=true]")
            .removeClass("validation-summary-errors")
            .addClass("validation-summary-valid")
            .find("ul").empty();

        $form.find("[data-valmsg-replace]")
            .removeClass("field-validation-error")
            .addClass("field-validation-valid")
            .empty();
    });

    // Bind confirm message for delete payment
    $(".js-delete-payment").bind("click", function (e) {
        if (confirm("Do you want to delete this payment?")) {
            return true;
        }

        e.preventDefault();
    });
});