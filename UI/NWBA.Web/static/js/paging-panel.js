﻿(function () {

    // Load the paging panel with selected paging URL
    function loadPagingPanel(targetContainer, callUrl) {

        // Making AJAX Get request to retrieve paged data
        $.get(callUrl,
             function (data) {
                 // Populate the data list with the updated list
                 var $report = $("#" + targetContainer);
                 $report.html(data);
             }
         ).fail(function (jqXHR, textStatus, errorThrown) {
             alert(textStatus);
         });

    }

    $(document).ready(function () {

        // Bind click on paging URL
        $(".js-paging-panel").on("click", ".js-paging-list li a", function (e) {
            e.preventDefault();

            var $this = $(this);
            var $parent = $this.parents(".js-paging-panel");

            // Make AJAX call to load the list
            loadPagingPanel($parent.attr("id"), $this.attr("href"));
        });
    });

})();