﻿(function () {

    // Load the account statement with URL
    function loadStatement(callUrl) {

        // Making AJAX Get request to retrieve statment
        $.get(callUrl,
             function (data) {
                 // Populate the statement list with the updated list
                 var $report = $("#transaction-list");
                 $report.html(data);
             }
         ).fail(function (jqXHR, textStatus, errorThrown) {
             alert(textStatus);
         });

    }

    $(document).ready(function () {

        // Bind select account action and load the statement
        $("#account-list").bind("change", function(e) {
            e.preventDefault();

            var $this = $(this);

            // Set the default statement to page one when select an account
            var reqUrl = $this.data("requrl") + "/" + $this.val() + "/1";
            loadStatement(reqUrl);
        });

        // Bind click on paging URL
        $("#transaction-list").on("click", ".js-paging-list li a", function(e) {
            e.preventDefault();

            var $this = $(this);

            // Make AJAX call to load the statement
            loadStatement($this.attr("href"));
        });
    });


})();