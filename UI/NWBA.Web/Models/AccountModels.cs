﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;
using NWBA.Domain.Interface;

namespace NWBA.Web.Models
{

    public class ValidAccountModel
    {
        public bool IsValidAccount { get; set; }
        public string ErrorMsg { get; set; }
    }

    public class LoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class ChangePasswordModel : IChangePassword
    {
        public bool PasswordUpdated { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current Password")]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "New Password")]
        [StringLength(25, MinimumLength = 4)]
        public string NewPassword { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password")]
        [Compare("NewPassword")]
        [StringLength(25, MinimumLength = 4)]
        public string ConfirmPassword { get; set; }
    }

    public class AccountStatmentModel : ValidAccountModel
    {
        public bool HasTransaction { get; set; }
        public IList<ITransaction> Transactions { get; set; }
        public PagingModel PageModel { get; set; }
    }

    public class ProfileModel : ICustomer, IAddress
    {
        public bool ValidProfile { get; set; }
        public bool ProfileUpdated { get; set; }
        public int CustomerID { get; set; }

        [Required]
        [Display(Name = "Your name")]
        public string CustomerName { get; set; }

        [Display(Name = "Tax File Number")]
        public string TFN { get; set; }

        public int AddressID { get; set; }

        [Required]
        public string Street { get; set; }

        [Required]
        public string City { get; set; }

        [Required]
        public string State { get; set; }

        [Required]
        [Display(Name = "Post Code")]
        public short PostCode { get; set; }

        [Required]
        [Display(Name = "Phone number")]
        public string Phone { get; set; }
    }


}
