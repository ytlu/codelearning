﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NWBA.Domain.Interface;

namespace NWBA.Web.Models
{
    public class CustomListModel
    {
        public string ErrorMsg { get; set; }
        public bool IsValidAccount { get; set; }
        public IList<ICustomer> CustomerList { get; set; }
        public bool HasCustomer { get; set; }
         public PagingModel PagingModel { get; set; }
    }

    public class CustomerAccountModel
    {
        public ProfileModel CustomerProfile { get; set; }
        public IList<IAccount> CustomerAccounts { get; set; }
    }

    public class CustomerBillPayModel
    {
        public string ErrorMsg { get; set; }
        public IList<IAdminBillPayRecord> BillPayRecords { get; set; }
        public bool HasBillPay { get; set; }
        public PagingModel PagingModel { get; set; }
    }
}