﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using NWBA.Domain.Interface;
using NWBA.Domain.Entity;

namespace NWBA.Web.Models
{
    public class TransactionModel : ITransaction
    {
        public IList<IAccount> AvailableAccounts { get; set; }

        public int TransactionID { get; set; }

        [Required]
        [Display(Name = "Transaction Type")]
        public string TransactionType { get; set; }

        public string TransactionTypeDisplay { get; set; }

        [Required]
        [Display(Name = "Select Account")]
        [Range(1, int.MaxValue, ErrorMessage = "Please select a valid account")]
        public int AccountNumber { get; set; }

        [Required]
        [Display(Name = "To Account")]
        [Range(1, int.MaxValue, ErrorMessage = "Please select a valid account")]
        public int? DestAccount { get; set; }

        [Required]
        [Display(Name = "Amount")]
        [Range(1, 99999)]
        [DataType(DataType.Currency)]
        public decimal Amount { get; set; }

        [Display(Name = "Comment")]
        public string Comment { get; set; }

        public DateTime ModifyDate { get; set; }
    }

    public class ScheduleBillPayModel : IBillPay
    {
        public IList<IPayee> Payees { get; set; }
        public IList<IAccount> AvailableAccounts { get; set; }

        public bool IsValidRecord { get; set; }
        public string ErrorMsg { get; set; }
        public string ActionMsg { get; set; }

        public int BillPayID { get; set; }

        [Required]
        [Display(Name = "Select Account")]
        [Range(1, int.MaxValue, ErrorMessage = "Please select a valid account")]
        public int AccountNumber { get; set; }
        
        [Required]
        [Display(Name = "To Payee")]
        [Range(1, int.MaxValue, ErrorMessage = "Please select a valid account")]
        public int PayeeID { get; set; }

        [Required]
        [Display(Name = "Amount")]
        [Range(1, 99999)]
        [DataType(DataType.Currency)]
        public decimal Amount { get; set; }

        [Required]
        [Display(Name = "Scheduled Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        [DataType(DataType.Date)]
        public DateTime ScheduleDate { get; set; }

        [Required]
        [Display(Name = "Period")]
        public string Period { get; set; }

        public DateTime ModifyDate { get; set; }
    }

    public class BillingModel : BillPayRecord, IBillPayRecord
    {
        public string ErrorMsg { get; set; }

        public PagingModel PagingModel { get; set; }
    }

}