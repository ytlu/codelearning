﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NWBA.Domain.Interface;

namespace NWBA.Web.Models
{
    public class PagingModel
    {
        public string ActionName { get; set; }
        public string ControllerName { get; set; }
        public string ID { get; set; }
        public IPageModel PageModel { get; set; }
    }
}