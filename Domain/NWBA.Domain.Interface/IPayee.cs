﻿namespace NWBA.Domain.Interface
{
    public interface IPayee
    {
        int PayeeID { get; set; }
        string PayeeName { get; set; }
        int AddressID { get; set; }
    }
}