﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NWBA.Domain.Interface
{
    public interface IBillPay
    {
        int BillPayID { get; set; }
        int AccountNumber { get; set; }
        int PayeeID { get; set; }
        decimal Amount { get; set; }
        System.DateTime ScheduleDate { get; set; }
        string Period { get; set; }
        System.DateTime ModifyDate { get; set; }
    }
}
