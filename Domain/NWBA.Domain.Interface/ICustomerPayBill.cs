﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NWBA.Domain.Interface
{
    public interface ICustomerPayBill
    {
        int BillPayID { get; set; }
        int AccountNumber { get; set; }
        decimal Balance { get; set; }
        string Period { get; set; }
        string PeriodDisplay { get; set; }

        string PayeeName { get; set; }
        decimal Amount { get; set; }
        System.DateTime ScheduleDate { get; set; }
    }
}
