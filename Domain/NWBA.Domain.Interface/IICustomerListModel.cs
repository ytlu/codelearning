﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NWBA.Domain.Interface
{
    public interface ICustomerListModel
    {
        bool HasCustomer { get; set; }
        IPageModel PageModel { get; set; }
        IList<ICustomer> Customers { get; set; }
    }
}
