﻿using System.Collections.Generic;

namespace NWBA.Domain.Interface
{
    public interface IBillPayRecord
    {
        bool HasRecord { get; set; }
        IPageModel PageModel { get; set; }
        IList<ICustomerPayBill> BillPays { get; set; }
    }
}
