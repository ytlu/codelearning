﻿namespace NWBA.Domain.Interface
{
    public interface IAddress
    {
        int AddressID { get; set; }
        string Street { get; set; }
        string City { get; set; }
        string State { get; set; }
        short PostCode { get; set; }
        string Phone { get; set; }
    }
}