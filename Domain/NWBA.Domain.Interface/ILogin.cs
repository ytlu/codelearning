﻿namespace NWBA.Domain.Interface
{
    public interface ILogin
    {
        int LoginID { get; set; }
        string UserID { get; set; }
        string Password { get; set; }
        System.DateTime ModifyDate { get; set; }
    }
}