﻿namespace NWBA.Domain.Interface
{
    public interface IAccount
    {
        int AccountNumber { get; set; }
        string AccountType { get; set; }
        string AccountTypeDisplay { get; set; }
        decimal Balance { get; set; }
        int CustomerID { get; set; }
        System.DateTime ModifyDate { get; set; }

    }
}