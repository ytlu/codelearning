﻿using System.Threading;

namespace NWBA.Domain.Interface
{
    public interface IPageModel
    {
        int StartPage { get; set; }
        int EndPage { get; set; }
        int PageSize { get; set; }
        int SelectedPage { get; set; }
        int TotalRecords { get; set; }
        int RecordFrom { get; set; }
        int RecordTo { get; set; }
        int TotalPages { get; set; }

        bool HasPrevious { get; set; }
        bool HasNext { get; set; }
        bool ShowPaging { get; set; }
    }
}