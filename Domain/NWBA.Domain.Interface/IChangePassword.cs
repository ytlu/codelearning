﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NWBA.Domain.Interface
{
    public interface IChangePassword
    {
        string Password { get; set; }
        string NewPassword { get; set; }
        string ConfirmPassword { get; set; }

    }
}
