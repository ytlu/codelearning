﻿namespace NWBA.Domain.Interface
{
    public interface IRecordRangeModel
    {
        int RecordFrom { get; set; }
        int NumberOfItem { get; set; }
    }
}