﻿using System;

namespace NWBA.Domain.Interface
{
    public interface ITransactionResult
    {
        int AccountNumber { get; set; }
        string TransactionTypeDesc { get; set; }
        int? DestAccountNumber { get; set; }
        decimal AccountBalance { get; set; }
        decimal Amount { get; set; }
        int ReceiptNumber { get; set; }
        DateTime TransactionDate { get; set; }
        string Comment { get; set; }
    }
}