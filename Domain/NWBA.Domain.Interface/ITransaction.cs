﻿namespace NWBA.Domain.Interface
{
    public interface ITransaction
    {
        int TransactionID { get; set; }
        string TransactionType { get; set; }
        string TransactionTypeDisplay { get; set; }
        int AccountNumber { get; set; }
        int? DestAccount { get; set; }
        decimal Amount { get; set; }
        string Comment { get; set; }
        System.DateTime ModifyDate { get; set; }
    }
}