﻿namespace NWBA.Domain.Interface
{
    public interface ICustomer
    {
        int CustomerID { get; set; }
        string CustomerName { get; set; }
        string TFN { get; set; }
    }
}