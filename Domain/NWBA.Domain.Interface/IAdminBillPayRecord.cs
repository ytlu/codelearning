﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NWBA.Domain.Interface
{
    public interface IAdminBillPayRecord
    {
        int CustomerID { get; set; }
        string CustomerName { get; set; }
        int AccountNumber { get; set; }
        decimal Balance { get; set; }
        int BillPayID { get; set; }
        string PayeeName { get; set; }
        string Period { get; set; }
        string PeriodDisplay { get; set; }
        decimal Amount { get; set; }
        bool AllowPause { get; set; }
        DateTime ScheduleDate { get; set; }
    }
}
