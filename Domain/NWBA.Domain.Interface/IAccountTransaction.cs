﻿using System.Collections.Generic;

namespace NWBA.Domain.Interface
{
    public interface IAccountTransaction
    {
        bool HasTransaction { get; set; }
        IPageModel PageModel { get; set; }
        IList<ITransaction> Transactions { get; set; }
    }
}