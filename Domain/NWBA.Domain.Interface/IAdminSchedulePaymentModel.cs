﻿using System.Collections.Generic;

namespace NWBA.Domain.Interface
{
    public interface IAdminSchedulePaymentModel
    {
        bool HasBillPay { get; set; }
        IPageModel PageModel { get; set; }
        IList<IAdminBillPayRecord> BillPayRecords { get; set; }
    }
}
