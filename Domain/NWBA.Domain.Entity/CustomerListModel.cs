﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NWBA.Domain.Interface;

namespace NWBA.Domain.Entity
{
    public class CustomerListModel : ICustomerListModel
    {
        public bool HasCustomer { get; set; }
        public IPageModel PageModel { get; set; }
        public IList<ICustomer> Customers { get; set; } 
    }
}
