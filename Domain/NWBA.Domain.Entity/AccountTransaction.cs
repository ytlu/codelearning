﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NWBA.Domain.Interface;

namespace NWBA.Domain.Entity
{
    public class AccountTransaction : IAccountTransaction
    {
        public bool HasTransaction { get; set; }
        public IPageModel PageModel { get; set; }
        public IList<ITransaction> Transactions { get; set; } 
    }
}
