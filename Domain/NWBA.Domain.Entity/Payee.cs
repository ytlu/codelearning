﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NWBA.Domain.Interface;

namespace NWBA.Domain.Entity
{
    public class Payee : IPayee
    {
        public int PayeeID { get; set; }
        public string PayeeName { get; set; }
        public int AddressID { get; set; }

    }
}
