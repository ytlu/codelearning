﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NWBA.Domain.Interface;

namespace NWBA.Domain.Entity
{
    public class TransactionResult : ITransactionResult
    {
        public int AccountNumber { get; set; }
        public string TransactionTypeDesc { get; set; }
        public int? DestAccountNumber { get; set; }
        public decimal AccountBalance { get; set; }
        public decimal Amount { get; set; }
        public int ReceiptNumber { get; set; }
        public DateTime TransactionDate { get; set; }
        public string Comment { get; set; }
    }
}
