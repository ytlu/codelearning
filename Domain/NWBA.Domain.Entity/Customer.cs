﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NWBA.Domain.Interface;

namespace NWBA.Domain.Entity
{
    public class Customer : ICustomer
    {
        public int CustomerID { get; set; }
        public string CustomerName { get; set; }
        public string TFN { get; set; }
    }
}
