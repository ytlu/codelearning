﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NWBA.Domain.Interface;

namespace NWBA.Domain.Entity
{

    public class BillPay : IBillPay
    {
        public int BillPayID { get; set; }
        public int AccountNumber { get; set; }
        public int PayeeID { get; set; }
        public decimal Amount { get; set; }
        public System.DateTime ScheduleDate { get; set; }
        public string Period { get; set; }
        public System.DateTime ModifyDate { get; set; }
    }
}
