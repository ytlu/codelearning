﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NWBA.Domain.Interface;

namespace NWBA.Domain.Entity
{


    public class CustomerPayBill : ICustomerPayBill
    {
        public int BillPayID { get; set; }
        public int AccountNumber { get; set; }
        public decimal Balance { get; set; }
        public string Period { get; set; }
        public string PeriodDisplay { get; set; }
        public string PayeeName { get; set; }
        public decimal Amount { get; set; }
        public System.DateTime ScheduleDate { get; set; }
    }
}
