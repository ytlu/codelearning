﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NWBA.Domain.Interface;

namespace NWBA.Domain.Entity
{
    public class PageModel : IPageModel
    {
        public int StartPage { get; set; }
        public int EndPage { get; set; }

        public int PageSize { get; set; }
        public int SelectedPage { get; set; }

        public int TotalRecords { get; set; }

        public int RecordFrom { get; set; }
        public int RecordTo { get; set; }

        public int TotalPages { get; set; }
        public bool HasPrevious { get; set; }
        public bool HasNext { get; set; }
        public bool ShowPaging { get; set; }
    }
}
