﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NWBA.Domain.Interface;

namespace NWBA.Domain.Entity
{
   public class Account : IAccount
   {
        public int AccountNumber { get; set; }
        public string AccountType { get; set; }
        public string AccountTypeDisplay { get; set; }
       public decimal Balance { get; set; }
       public int CustomerID { get; set; }
       public System.DateTime ModifyDate { get; set; }


    }
}
