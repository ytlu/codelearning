﻿using System.Collections.Generic;
using NWBA.Domain.Interface;

namespace NWBA.Domain.Entity
{
    public class AdminSchedulePaymentModel : IAdminSchedulePaymentModel
    {
        public bool HasBillPay { get; set; }
        public IPageModel PageModel { get; set; }
        public IList<IAdminBillPayRecord> BillPayRecords { get; set; }
    }
}
