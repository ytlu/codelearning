﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NWBA.Domain.Interface;

namespace NWBA.Domain.Entity
{
    public class RecordRangeModel : IRecordRangeModel
    {
        public int RecordFrom { get; set; }
        public int NumberOfItem { get; set; }
       
    }
}
