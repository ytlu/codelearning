﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NWBA.Domain.Interface;

namespace NWBA.Domain.Entity
{
    public class Transaction : ITransaction
    {
        public int TransactionID { get; set; }
        public string TransactionType { get; set; }
        public string TransactionTypeDisplay { get; set; }
        public int AccountNumber { get; set; }
        public int? DestAccount { get; set; }
        public decimal Amount { get; set; }
        public string Comment { get; set; }
        public System.DateTime ModifyDate { get; set; }
    }
}
