﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NWBA.Domain.Interface;

namespace NWBA.Domain.Entity
{
    public class Login : ILogin
    {
        public int LoginID { get; set; }
        public string UserID { get; set; }
        public string Password { get; set; }
        public DateTime ModifyDate { get; set; }
    }
}
