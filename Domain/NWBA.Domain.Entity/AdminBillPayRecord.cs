﻿using System;
using NWBA.Domain.Interface;

namespace NWBA.Domain.Entity
{
    public class AdminBillPayRecord : IAdminBillPayRecord
    {
        public int CustomerID { get; set; }
        public string CustomerName { get; set; }
        public int AccountNumber { get; set; }
        public decimal Balance { get; set; }
        public int BillPayID { get; set; }
        public string PayeeName { get; set; }
        public string Period { get; set; }
        public string PeriodDisplay { get; set; }
        public decimal Amount { get; set; }
        public bool AllowPause { get; set; }
        public DateTime ScheduleDate { get; set; }
    }
}
